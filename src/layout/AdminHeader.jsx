import React, { Component } from 'react';
import { withRouter } from 'react-router';

class AdminHeader extends Component {
    constructor(props) {
        super(props);
    } 
    

    logout = () => {
        localStorage.clear('token');
        this.props.history.push('/sign-in');
    };
    render(){
        return(
            <>                
                <div className="row border-bottom">
                    <nav className="navbar navbar-static-top white-bg" role="navigation">
                        <div className="navbar-header">
                            
                           
                        </div>
                        <ul className="nav navbar-top-links navbar-right">
                            <li>
                                <span className="m-r-sm text-muted welcome-message">Welcome to Auto Merchant.</span>
                            </li>
                           
                            <li>

                                <a className="p-2 login-item" href="#" role="button"  onClick={ () => this.logout() } ><i className="fa fa-sign-out"></i> Logout</a> 
                                {/*<a href="login.html">
                                    <i className="fa fa-sign-out"></i> Log out
                                </a>*/}
                            </li>
                          
                        </ul>
                    </nav>
                </div>
            </>
        )
    }
}

export default withRouter(AdminHeader);