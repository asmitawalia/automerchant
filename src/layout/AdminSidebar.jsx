import React, { Component } from 'react';
import { NavLink, Redirect } from 'react-router-dom';


class AdminSidebar extends Component {
    render(){
        return(
            <>
                <nav className="navbar-default navbar-static-side" role="navigation">
                        <div className="sidebar-collapse">
                            <ul className="nav metismenu" id="side-menu">
                                <li className="nav-header">
                                    <div className="dropdown profile-element">
                                        <img alt="image" className="rounded-circle" src="/assets/images/Richard.jpg" />
                                        <a data-toggle="dropdown" className="dropdown-toggle" href="#">
                                            <span className="block m-t-xs font-bold">Richard Smith</span>
                                            <span className="text-muted text-xs block">CEO</span>
                                        </a>
                                        <ul className="dropdown-menu animated fadeInRight m-t-xs">
                                            <li><a className="dropdown-item" href="profile.html">Profile</a></li>
                                            <li><a className="dropdown-item" href="contacts.html">Contacts</a></li>
                                            <li><a className="dropdown-item" href="mailbox.html">Mailbox</a></li>
                                            <li className="dropdown-divider"></li>
                                            <li><a className="dropdown-item" href="login.html">Logout</a></li>
                                        </ul>
                                    </div>
                                    <div className="logo-element">
                                        IN+
                                    </div>
                                </li>
                                <li className="">
                                    <NavLink className="nav-link" to="/dashboard"><i className="fa fa-th-large"></i><span className="nav-label">Dashboards</span></NavLink>
                                    {/*<a href="index.html" aria-expanded="false"><i className="fa fa-th-large"></i> <span className="nav-label">Dashboards</span> <span className="fa arrow"></span></a>
                                    <ul className="nav nav-second-level collapse" aria-expanded="false">
                                        <li><a href="index.html">Dashboard v.1</a></li>
                                        <li className="active"><a href="dashboard_2.html">Dashboard v.2</a></li>
                                        <li><a href="dashboard_3.html">Dashboard v.3</a></li>
                                        <li><a href="dashboard_4_1.html">Dashboard v.4</a></li>
                                        <li><a href="dashboard_5.html">Dashboard v.5 </a></li>
                                    </ul>*/}
                                </li>
                                <li>
                                    <NavLink className="nav-link" to="/vehicles"><i className="fa fa-car"></i><span className="nav-label">Cars 1</span></NavLink>
                                </li>
                                <li>
                                    <NavLink className="nav-link" to="/vehiclesnew"><i className="fa fa-car"></i><span className="nav-label">Cars 2</span></NavLink>
                                </li>
                                <li>
                                    <NavLink className="nav-link" to="/leads"><i className="fa fa-bar-chart-o"></i><span className="nav-label">Leads</span></NavLink>
                                </li>
                                <li>
                                    <NavLink className="nav-link" to="/settings"><i className="fa fa-bar-chart-o"></i><span className="nav-label">Settings</span></NavLink>
                                </li>
                                
                            </ul>

                        </div>
                </nav>
            </>
        )
    }
}

export default AdminSidebar;