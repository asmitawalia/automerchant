import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
class EmptyLayout extends Component {
    render() {
        // console.log(this.props.location.pathname)
        if(this.props.location.pathname === '/'){
            return (
                <>
                  <Header/>
                  <section class="banner">
                    {/* <div class="container"> */}
                        {this.props.children}
                    {/* </div> */}
                  </section>
                    <Footer/>
                </>
            );
        }else{
            return (
                <>
                  <Header/>
                  <section class="banner">
                    <div class="container">
                        {this.props.children}
                    </div>
                  </section>
                    <Footer/>
                </>
            );
        }
        
    }
}
export default EmptyLayout;