import React, { Component } from 'react';
class CompleteEmptyLayout extends Component {
    render() {
        return (
            <>
            <div className="login-body-wrapper login-body-login">
                {this.props.children}
            </div>
            </>
        );
    }
}

export default CompleteEmptyLayout;