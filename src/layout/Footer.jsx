import React, { Component } from 'react';
import { withRouter } from 'react-router';
import Login from '../components/Login';
import SignUp from '../components/SignUp';
// import DogDetail from '../components/DogDetail';

class Footer extends Component {
   
    render() {
        return (
            <>
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center head-title">
                                    <h2>Get in touch</h2>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact-info">
                                    <i class="fa fa-envelope-o"></i>
                                    <strong>Email</strong>
                                    <span>hello@automerchantuk.ie</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact-info">
                                    <i class="fa fa-mobile"></i>
                                    <strong>Phone</strong>
                                    <span>+123 456 7890</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contact-info">
                                    <i class="fa fa-envelope"></i>
                                    <strong>Office</strong>
                                    <span>51 Braken Rd, Sandyford<br/>Business Park, Sandyford.</span>
  				                </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center social-icon">
                                    <ul class="nav justify-content-center">
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-instagram"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-youtube"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-youtube"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 offset-md-3">
                                <div class="copyright">
                                    <p>Copyright @ 2020. Auto Merchant UK- All Right Reserved</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
            </>
        );
    }
}

export default withRouter(Footer);