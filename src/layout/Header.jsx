import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import parseJwt from './../store/helpers/common';
import jwt_decode from 'jwt-decode';
import { getUserdetails } from './../store/actions/userActions';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            role: '',
            isLoggedIn: false,
            username: '',
            user_id: '',
            singleuser: []
        }
    }  
    componentDidMount() {
      
    }
    
    render() {
        return (
            <>
                <header className="header">
                    <nav className="navbar navbar-expand-md navbar-dark" data-spy="affix" data-offset-top="200" role="navigation">
                        <div className="container">
                            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#head-navbar">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <NavLink className="nav-link navbar-brand text-white" to="/"><strong>Auto Merchant UK</strong></NavLink>
                            {/*<a className="navbar-brand text-white" href="#"></a>
                             <!-- Below line for Right side Logo --> */}
                            {/* <!-- <a className="navbar-brand" href="#">Navbar</a> --> */}
                            <div className="collapse navbar-collapse" id="head-navbar">
                                <ul className="navbar-nav ml-md-auto">
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/">Home</NavLink>

                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/vehicle-lists">Used Cars 1</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/vehicle-lists-new">Used Cars 2</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="#">Finance</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/about">About</NavLink>

                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="/contact">Contact</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link" to="#"><i className="fa fa-search"></i></NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="#"><i className="fa fa-facebook"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {/* <!---End/.container--> */}
                    </nav>
                </header>
            </>
            );
        }
}

export default Header;

