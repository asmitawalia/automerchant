import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';

class About extends Component {
    render() {
        return (
            <>
            <div className="cardealer mt-5">
	          	<div className="row">
					<div className="col-md-12">
						<div className="text-center top-head">
							<h2 className="text-uppercase">Auto Merchant UK</h2>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the galley of type and scrambled it to make a type specimen book.</p>
						</div>
					</div>
				</div>
				<div className="row mt-5">
					<div className="col-md-6">
						<div className="left-image">
							<img src="assets/images/img2.jpg" alt="car-image" className="img-fluid"/>
						</div>
					</div>
					<div className="col-md-6">
						<div className="right-content">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

						</div>
					</div>
				</div>
				<div className="row mt-5">
					<div className="col-md-3">
						<div className="car-box">
							<div className="image">
								<span>
									{/* <!-- <img src="assets/images/ic_channels.png" alt="brand-imgage" className=""> --> */}
									<i className="fa fa-car"></i>
								</span>
							</div>
							<div className="content">
								<strong className="text-uppercase">All Brands</strong>
								<p>Lorem Ipsum simply and typesetting industry.</p>
							</div>
						</div>
					</div>
					<div className="col-md-3">
						<div className="car-box">
							<div className="image">
								<span>
									<i className="fa fa-comments-o"></i>
								</span>
							</div>
							<div className="content">
								<strong className="text-uppercase">Free Support</strong>
								<p>Lorem Ipsum simply and typesetting industry.</p>
							</div>
						</div>
					</div>
					<div className="col-md-3">
						<div className="car-box">
							<div className="image">
								<span>
									<i className="fa fa-key"></i>
								</span>
							</div>
							<div className="content">
								<strong className="text-uppercase">Dealership</strong>
								<p>Lorem Ipsum simply and typesetting industry.</p>
							</div>
						</div>
					</div>
					<div className="col-md-3">
						<div className="car-box">
							<div className="image">
								<span>
									<i className="fa fa-address-card-o"></i>
								</span>
							</div>
							<div className="content">
								<strong className="text-uppercase">Affordable</strong>
								<p>Lorem Ipsum simply and typesetting industry.</p>
							</div>
						</div>
					</div>
				</div> 
			</div>
			<div className="our-history" id="fourth">
				<div className="row history">
					<div className="col-md-12">
						<div className="text-center top-head">
							<p>Lorem Ipsum simply and typesetting industry.</p>
							<h2 className="text-uppercase">Our Steps</h2>
						</div>
					</div>
				</div>

			    <div className="row">
			    	<div className="col-md-12">
			    		 <div className="image">
			            	<div className="row">
			                	<div className="col-md-6">
			            			<div className="left-content">
			                			<h4>Step 1</h4>
			                    		<p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente.</p>
			            			</div>
			                    </div>
			                 	<div className="col-md-6">
			                    	<div className="right-content">
			                    		<div className="img"></div>
			                        	<span className="line"></span>
			                    	</div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <div className="row">
			    	<div className="col-md-12">
			        	<div className="sc">
			            	<div className="row">
			                	<div className="col-md-7">
			                    	<div className="img"></div>
			                    	<span className="line"></span>
			                    </div>
			                 	<div className="col-md-5">
			                 		<div className="left-content">
			                			<h4>Step 2</h4>
			                    		<p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente.</p>
			            			</div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>

			    <div className="row">
			    	<div className="col-md-12">
			    		<div className="image">
			            	<div className="row">
			                	<div className="col-md-6">
			            			<div className="left-content">
			                			<h4>Step 3</h4>
			                    		<p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente.</p>
			            			</div>
			                    </div>
			                 	<div className="col-md-6">
			                    	<div className="right-content">
			                    		<div className="img img3"></div>
			                        	<span className="line"></span>
			                    	</div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			    <div className="row">
			    	<div className="col-md-12">
			    		<div className="sc">
			            	<div className="row">
			                	<div className="col-md-7">
			                    	<div className="img img4"></div>
			                    	<span className="line line2"></span>
			                    </div>
			                 	<div className="col-md-5">
			                 		<div className="left-content">
			                			<h4>Step 4</h4>
			                    		<p className="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente.</p>
			            			</div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<div className="vehicle-stock">
				<div className="row">
					<div className="col-md-3">
						<div className="stock-box">
							<strong className="head">Vehicles in Stock</strong>
							<div className="content">
								<span>
									<i className="fa fa-taxi"></i>
								</span>
								<strong>5610</strong>
							</div>
						</div>
					</div>
					<div className="col-md-3">
						<div className="stock-box">
							<strong className="head">Dealer Revies</strong>
							<div className="content">
								<span>
									<i className="fa fa-commenting"></i>
								</span>
								<strong>856</strong>
							</div>
						</div>
					</div>
					<div className="col-md-3">
						<div className="stock-box">
							<strong className="head">Happy Customers</strong>
							<div className="content">
								<span>
									<i className="fa fa-user-circle-o"></i>
								</span>
								<strong>789</strong>
							</div>
						</div>
					</div>
					<div className="col-md-3">
						<div className="stock-box">
							<strong className="head">Awards</strong>
							<div className="content">
								<span>
									<i className="fa fa-trophy"></i>
								</span>
								<strong>356</strong>
							</div>
						</div>
					</div>
				</div>
			</div>
		    <div className="client-testimonial">
		     <div className="row">
				<div className="col-md-12 mb-4">
					<div className="text-center top-head">
						<p>Lorem Ipsum simply and typesetting industry.</p>
						<h2 className="text-uppercase">Our Testimonial</h2>
					</div>
				</div>
			  </div>
		      <div className="row">
		        <div className="col-md-4">
		          <div className="car-box">
		            <div className="imagebox">
		              <img src="assets/images/img3.jpg" alt="car-image" className="img-fluid" />
		            </div>
		            <div className="content-box">
		            	<div className="image">
		            		<img src="assets/images/tim-collins.jpg" alt="car-image" className="img-fluid" />
		            		<span className="title">Felicia Queen</span>
		            		<span className="title2">Auto Dealer</span>
		            	</div>
		            	<div className="reviews">
		            		<p>lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site ame.
		                  lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet.
		              		</p>
		              		<i className="fa fa-quote-right"></i>
		            	</div>
		            </div>
		          </div>
		        </div>
		        <div className="col-md-4">
		          <div className="car-box">
		            <div className="imagebox">
		             <img src="assets/images/img3.jpg" alt="car-image" className="img-fluid" />
		            </div>
		            <div className="content-box">
		            	<div className="image">
		            		<img src="assets/images/tim-collins.jpg" alt="car-image" className="img-fluid" />
		            		<span className="title">Melissa Doe</span>
		            		<span className="title2">Customer</span>
		            	</div>
		            	<div className="reviews">
		            		<p>lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site ame.
		                  lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet.
		              		</p>
		              		<i className="fa fa-quote-right"></i>
		            	</div>
		            </div>
		          </div>
		        </div>
		        <div className="col-md-4">
		          <div className="car-box">
		            <div className="imagebox">
		             <img src="assets/images/img3.jpg" alt="car-image" className="img-fluid" />
		            </div>
		            <div className="content-box">
		            	<div className="image">
		            		<img src="assets/images/tim-collins.jpg" alt="car-image" className="img-fluid" />
		            		<span className="title">Michael Bean</span>
		            		<span className="title2">Auto Dealer</span>
		            	</div>
		            	<div className="reviews">
		            		<p>lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site ame.
		                  lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet lorem ipsume doller site amet.
		              		</p>
		              		<i className="fa fa-quote-right"></i>
		            	</div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
        </>
        )
    }
}

export default withRouter(About);