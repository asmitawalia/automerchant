import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';

class Contact extends Component {
    render() {
        return (
            <>
            	<div className="cardealer gentin-touch"> 
		           	<div className="row">
						<div className="col-md-12 mb-4">
							<div className="text-center top-head">
								<p>Lorem Ipsum simply and typesetting industry.</p>
								<h2 className="text-uppercase">Let's Get in Touch!</h2>
							</div>
						</div>
					</div>

				  	<div className="row">
					  	<div className="col-md-3">
					  		<div className="info-box">
					  			<i className="fa fa-map-marker"></i>
					  			<strong>Address</strong>
					  			<span>Dublin, Ireland</span>
					  		</div>
					  	</div>
					  	<div className="col-md-3">
					  		<div className="info-box">
					  			<i className="fa fa-phone"></i>
					  			<strong>Phone</strong>
					  			<span>(007) 123 456 7890</span>
					  		</div>
					  	</div>
					  	<div className="col-md-3">
					  		<div className="info-box">
					  			<i className="fa fa-envelope"></i>
					  			<strong>Email</strong>
					  			<span>hello@automerchantuk.ie</span>
					  		</div>
					  	</div>
					  	<div className="col-md-3">
					  		<div className="info-box">
					  			<i className="fa fa-fax"></i>
					  			<strong>Fax</strong>
					  			<span>(007) 123 456 7890</span>
					  		</div>
					  	</div>
				  	</div>

				  	<div className="row mt-5">
					  	<div className="col-md-8">
					  		<div className="contact-info">
						  		<form action="">
								  <div className="form-group">
								    <div className="row">
								    	<div className="col-md-4">
								    		<input type="text" className="form-control" placeholder="Name*"/>
								    	</div>
								    	<div className="col-md-4">
								    		<input type="email" className="form-control" placeholder="Email*"/>
								    	</div>
								    	<div className="col-md-4">
								    		<input type="text" className="form-control" placeholder="Phone*"/>
								    	</div>
								    </div>
								    <div className="row mt-3">
								    	<div className="col-md-12">
								    		<textarea className="form-control" rows="8" placeholder="Comment*"></textarea>
								    	</div>
								    </div>
								  </div>
								  <div className=""><button type="button" className="btn btn-danger btn-block">Send</button></div>
								</form>
					  		</div>
					  	</div>
					  	<div className="col-md-4">
					  		<div className="opening-time">
					  			<h5>Opening Hours</h5>
					  			  <ul className="navbar-nav">
								    <li className="nav-item">
								      Monday<span>9:00 AM to 9:00 PM</span>
								    </li>
								    <li className="nav-item">
								      Tuesday<span>9:00 AM to 9:00 PM</span>
								    </li>
								    <li className="nav-item">
								      Wednessday<span>9:00 AM to 9:00 PM</span>
								    </li>
								    <li className="nav-item">
								      Thursday<span>9:00 AM to 9:00 PM</span>
								    </li>
								    <li className="nav-item">
								      Friday<span>9:00 AM to 9:00 PM</span>
								    </li>
								    <li className="nav-item">
								      Saturday<span>9:00 AM to 9:00 PM</span>
								    </li>
								    <li className="nav-item d-block last">
								      Sunday<span>Closed</span>
								    </li>
								 </ul>
					  		</div>
					  	</div>
				  	</div>

					<div className="row mt-5">
				  		<div className="col-md-4">
							<div className="car-box">
								<div className="image">
									<span>
										<i className="fa fa-clock-o"></i>
									</span>
								</div>
								<div className="content">
									<strong className="text-uppercase">Opening Hours</strong>
									<p>Lorem Ipsum simply and typesetting industry.</p>
								</div>
							</div>
						</div>
				  		<div className="col-md-4">
							<div className="car-box">
								<div className="image">
									<span>
										<i className="fa fa-futbol-o"></i>
										{/* <!-- <img src="web/images/ic_channels.png" alt="brand-imgage" className=""> --> */}
									</span>
								</div>
								<div className="content">
									<strong className="text-uppercase">Our Support Center</strong>
									<p>Lorem Ipsum simply and typesetting industry.</p>
								</div>
							</div>
						</div>
				  		<div className="col-md-4">
							<div className="car-box">
								<div className="image">
									<span>
										<i className="fa fa-clock-o"></i>
									</span>
								</div>
								<div className="content">
									<strong className="text-uppercase">Some Information</strong>
									<p>Lorem Ipsum simply and typesetting industry.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</>
        )
    }
}

export default withRouter(Contact);