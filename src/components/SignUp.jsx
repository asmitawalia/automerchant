import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { withRouter } from 'react-router';
import { apiBaseUrl } from './../store/helpers/common';
import SimpleReactValidator from 'simple-react-validator';
import { getstates } from './../store/actions/CommonActions';
import toastr from 'reactjs-toastr';
import 'reactjs-toastr/lib/toast.css';
// import { Redirect } from 'react-router-dom';
// import { useHistory } from "react-router-dom";

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.signupValidator = new SimpleReactValidator();
        this.state = {
            fname: '',
            lname: '',
            phone:'',
            store_name:'',
            state:'',
            cpassword:'',
            email: '',
            password: '',        
            loading: false,
            isSubmit: false,
            allstates: ''
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        e.preventDefault();
    }
    componentDidMount(){
        this.props.dispatch(getstates());
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            allstates: nextProps.states
        })
    }
    postData =(ev) => {
        ev.preventDefault();
        let validate = this.signupValidator;
        if(validate.allValid()){
            this.setState({ isSubmit: true });
            const { fname, lname, email, phone, password, cpassword, store_name, store_state } = this.state;
            const fullname = fname+' '+lname;
            const user_type = 'breeder';
            const method = 'manual';
            const request = new Request(`${apiBaseUrl}/signup`, {
                method: 'POST',
                body: JSON.stringify({ fullname, email, user_type, password, store_name, store_state, method}),
                headers: new Headers({ 'Content-Type': 'application/json' }),
            })
            if( password !== cpassword){
                this.setState({ isSubmit: false});
                toastr.error('Passwords Dont Match', { displayDuration: 2000 });
            } else {
                return fetch(request).then(res => res.json())
                .then((data) => {
                    if (data.ResponseCode === '1') {
                        toastr.success(data.ResponseText, { displayDuration: 2000 })
                        const request = new Request(`${apiBaseUrl}/user/login`, {
                            method: 'POST',
                            body: JSON.stringify({ email, password }),
                            headers: new Headers({ 'Content-Type': 'application/json' }),
                        })
                        return fetch(request).then(res => res.json()).then( (data) => {
                            if (data.token) {
                                localStorage.setItem('token', data.token);
                                this.setState({ isSubmit: true , loggedIn: true});
                                // this.props.history.push('/admin-dashboard');
                                document.getElementById("close").click();
                                // const//ry.push("/admin-dashboard");

                                window.location.reload();
                               
                            } else {
                                this.setState({ isSubmit: false , loggedIn: false});
                            }
                        }).catch((err) => {
                            this.setState({ isSubmit: false});  
                        })
                    } else {
                        toastr.error(data.ResponseText, { displayDuration: 2000 })
                        this.setState({ isSubmit: false});
                    }
                }).catch((err) => {
                    this.setState({ isSubmit: false});
                })
            }
        } else {
            validate.showMessages();
            this.forceUpdate();
        }
    }
    render(){
        return(
            <>
                <div className="modal fade" id="modal-container-signup" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <button type="button" className="close" id="close" data-dismiss="modal">&times;</button>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="login modal-dialog" role="document">
                                    <div className="login modal-content">                
                                        {/* <div className="login modal-header text-center">
                                            <h5 className="modal-titl text-center w-100" id="myModalLabel">LOGIN</h5>
                                        </div> */}
                                        <div className="modal-body">
                                            <div className="logincontainer">
                                                <div className="login-img-sec">
                                                    <img src="https://res.cloudinary.com/dahyfu60u/image/upload/v1592552776/assets/img/Golden_Retriever_DXE979_fnezeu.jpg" alt=""/>
                                                </div>
                                                <div className="login-form-sec">
                                                    <h4 className="title">Sign Up</h4>
                                                    <input className="form-input" type="text" name="fname" placeholder="First Name" value={this.state.fname} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('First Name', this.state.fname, 'required')}
                                                    <input className="form-input" type="text" name="lname" placeholder="Last Name" value={this.state.lname} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('Last Name', this.state.lname, 'required')}
                                                    <input className="form-input" type="text" name="email" placeholder="Email Address" value={this.state.email} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('Email Address', this.state.email, 'required|email')}
                                                    <input className="form-input" type="text" name="phone" placeholder="Phone" value={this.state.phone} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('Phone', this.state.phone, 'required|phone')}
                                                    <input className="form-input" type="text" name="store_name" placeholder="Store Name" value={this.state.store_name} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('Store Name', this.state.store_name, 'required')}
                                                    <select className="form-input" type="text" name="store_state" onChange={e => this.handleChange(e)}>
                                                        <option value="">Select your Store State</option>
                                                    {
                                                        this.state.allstates ?
                                                            this.state.allstates.map((post) => (
                                                                <option key={post.id} value={post.name}>{post.name}</option>
                                                            ))
                                                        : 
                                                            ''
                                                    }
                                                    </select>
                                                    {this.signupValidator.message('State', this.state.store_state, 'required')}
                                                    <input className="form-input" type="password" name="password" placeholder="Password" value={this.state.password} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('Password', this.state.password, 'required')}
                                                    <input className="form-input" type="password" name="cpassword" placeholder="Confirm Password" value={this.state.cpassword} onChange={e => this.handleChange(e)} />
                                                    {this.signupValidator.message('Confirm Password', this.state.cpassword, 'required')}
                                                    <button className="signup modal-action-btn" onClick={(ev) => this.postData(ev)} disabled={this.state.isSubmit} type="button">{this.state.isSubmit ? 'PLEASE WAIT..' : 'Signup'}</button>
                                                    <p className="text-muted">orConnect with Social Media</p>
                                                    <button className="twitter modal-action-btn" type="button">Sign in with Twitter</button>
                                                    <button className="facebook modal-action-btn" type="button">Sign in with Facebook</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
const mapStateToProps = (state) => ({
    statesloading: state.common.statesloading,
    states: state.common.states
})
export default connect(mapStateToProps)(SignUp);
// export default withRouter(SignUp);