import React, { Component } from 'react';
import { withRouter } from 'react-router';
import SimpleReactValidator from 'simple-react-validator';
import { apiBaseUrl } from './../store/helpers/common';
import toastr from 'reactjs-toastr';
import 'reactjs-toastr/lib/toast.css';


class SignIn extends Component {
    constructor(props) {
    	super(props);
    	this.LoginValidator = new SimpleReactValidator();
        this.state = {
        	email:'',
            password: '',
            isSubmit: false
        }
    }
    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        e.preventDefault();
    }
    SignIn = (e) => {
    	let validate = this.LoginValidator;
        if(validate.allValid()){
            this.setState({ isSubmit: true });
            const {email, password } = this.state;
            const request = new Request(`${apiBaseUrl}/login`, {
                method: 'POST',
                headers: new Headers({ 'Content-Type': 'application/json' }),
                body: JSON.stringify({ email, password }),
            })
            return fetch(request).then(res => res.json()).then(data => { 
                if(data.ResponseCode == 1){
                    toastr.success(data.ResponseText, { displayDuration: 1500 });
                    localStorage.setItem('token', data.token);
                    this.setState({ isSubmit: false});
                    this.props.history.push('/dashboard');
                    
                }else{
                    toastr.error(data.ResponseText, { displayDuration: 1500 })
                    this.setState({ isSubmit: false});
                }
            }).catch((err) => {
                console.log(err);
                this.setState({ isSubmit: false});  
            })
        }else{
        	validate.showMessages();
            this.forceUpdate();
        }
    }
	render(){
		return(
			<div class="middle-box text-center loginscreen animated fadeInDown">
        		<div>
            		<div>
                		<h1 class="logo-name">AM</h1>
            		</div>
            		<h3>Welcome to Auto Merchant UK</h3>
		            <p>Precisely prepared for Buying UK Cars Online from Ireland.
		            </p>
		            <p>Login in. To see it in action.</p>
		            <form class="m-t" role="form" action="index.html">
		                <div class="form-group">
		                    <input type="email" name="email" class="form-control" value={this.state.email} placeholder="Username" onChange={ e => this.handleChange(e)} />
		                    {this.LoginValidator.message('Email', this.state.email, 'required|email')}
		                </div>
		                <div class="form-group">
		                    <input type="password" name="password" class="form-control" value={this.state.password} placeholder="Password" onChange={ e => this.handleChange(e)} />
		                    {this.LoginValidator.message('Password', this.state.password, 'required')}
		                </div>
		                <button type="button" class="btn btn-primary block full-width m-b" onClick={ ev => this.SignIn(ev)} disabled={this.state.isSubmit} >{this.state.isSubmit ? 'Please Wait....' : 'Login'}</button>

		                {/*<a href="#"><small>Forgot password?</small></a>
		                <p class="text-muted text-center"><small>Do not have an account?</small></p>
		                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>*/}
		            </form>
        		</div>
    		</div>
		)
	}
}

export default withRouter(SignIn);