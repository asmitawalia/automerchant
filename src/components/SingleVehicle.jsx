import React, { Component } from 'react';
import {Button, Form, Col, Row} from 'react-bootstrap';
import SimpleReactValidator from 'simple-react-validator';
import { apiBaseUrl } from './../store/helpers/common';
import toastr from "reactjs-toastr";
import { getcar } from '../store/actions/CarActions';
import { connect } from 'react-redux';
import 'react-phone-number-input/style.css';
import PhoneInput from 'react-phone-number-input';
import $ from "jquery";
class SingleVehicle extends Component {
	constructor(props) {
		super(props);
        this.formValidator =  new SimpleReactValidator();
		this.state = {
			cardata:'',
			car_images:'',
			car_id:'',
			Name:'',
			Email:'',
			Phone:'',
			twelvemonthwarrenty:'',
			premiummax395:'',
			vrt_proccessing:"0",
			transferuktodub:"0",
			homedelivry:"0",
			tnc:'',
			isSubmit: false
		}
	}
	handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    handleCheckbox = (e) => {
    	let value_checked = "0";
    	if(e.target.checked == true){
    		value_checked = "1";
    	}else if(e.target.name == 'tnc'){
    		value_checked = '';
    	}
    	this.setState({
            [e.target.name]: value_checked
        });
    }
	componentDidMount() {
        const car_id = this.props.match.params.id;
        this.setState({car_id: car_id});
        this.props.dispatch(getcar(car_id));
	}

    componentWillReceiveProps(nextProps) {
        this.setState({
            cardata: nextProps.cardata,
            car_images: nextProps.cardata.decoded_images,
        });
        console.log('cardata :'+JSON.stringify(nextProps.cardata));
    }
    setphone = (e) => {
        this.setState({Phone:e});
    }
    submitform = (ev) => {
		let validate = this.formValidator;
        if(validate.allValid()){
        	this.setState({isSubmit:true});
            const {Name, Email, Phone, twelvemonthwarrenty, premiummax395, vrt_proccessing, transferuktodub, homedelivry, tnc, car_id} = this.state;
            const request =  new Request(`${apiBaseUrl}/submit-form`,{
                method: 'POST',
                headers: new Headers({ 'Content-Type': 'application/json','X-Auth-Token': `${localStorage.getItem('token')}`}),
                body: JSON.stringify({Name, Email, Phone, twelvemonthwarrenty, premiummax395, vrt_proccessing, transferuktodub, homedelivry, tnc, car_id})
            });
            return fetch(request).then(res => res.json()).then((data) => {
                console.log('data :'+JSON.stringify(data.ResponseCode));
                if(data.ResponseCode == 1){
                    toastr.success('Form Submitted Successfully, We will get back to you soon.', { displayDuration: 3000 });
                    this.setState ({isSubmit : false})
                    window.location.reload();
                }
            }).catch(err => {
                this.setState ({isSubmit : false})
                console.log('err :'+err);
            })
        }else{
        	console.log('tnc: '+this.state.tnc)
        	validate.showMessages();
            this.forceUpdate();
        }
    }
	getVehicle = (car_id) => {
		$('html,body').animate({ scrollTop: 0 }, 'slow');
		this.setState({car_id: car_id});
        this.props.dispatch(getcar(car_id));
	}
    render() {
    	const {cardata, car_images} = this.state;

    	let i = 0;
    	let j = 0;
        return (
			<>
				{
					this.props.carloading 
	  					? 	
	  						<div className="alltourticksloader">
                            	<img className="loader_img" src="/assets/images/straight-loader.gif" />
                        	</div>
                        : cardata
                            ? 
                            	<>
	                            	<div className="prestise pt-5 pb-4">				
										<div className="row">
											<div className="col-md-12">
												<h2>{cardata.year} {cardata.car_name} {cardata.model} </h2>
												{/* <p>{cardata.vehicle_overview}</p> */}
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-question-circle"
														aria-hidden="true"></i>
													Request More info</button>
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-tag" aria-hidden="true"></i>
													Make an offer</button>
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-cog" aria-hidden="true"></i>
													Schedule Test Drive</button>
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-envelope"
														aria-hidden="true"></i>
													Email to a Friend</button>
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-list-alt"
														aria-hidden="true"></i>
													Financial Form</button>
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-file-pdf-o"
														aria-hidden="true"></i>
													PDF Brochure</button>
												<button type="button" className="btn btn-outline-secondary"><i className="fa fa-print"
														aria-hidden="true"></i>
													Print</button>
											</div>
										</div>				
									</div>

									<div className="social-links">
										<div className="row">
											<div className="col-md-12">
												<div className="border border-grey group w-100 d-md-flex py-1">
													<div className="col-lg-6 col-md-6 col-sm-12">
														<button type="button" className="btn btn-light"><i className="fa fa-play" aria-hidden="true"></i>
															Vehicle videos</button>

														<button type="button" className="btn btn-light"><i className="fa fa-compress"
																aria-hidden="true"></i>
															Add to compare</button>

													</div>
													<div className="col-lg-6 col-md-6 col-sm-12">
														<div className="social-lists align-items-center justify-content-center d-flex pull-right pt-2">
															Share : <i className="fa fa-facebook" aria-hidden="true"></i>
															<i className="fa fa-twitter" aria-hidden="true"></i>
															<i className="fa fa-linkedin" aria-hidden="true"></i>
															<i className="fa fa-google-plus" aria-hidden="true"></i>
															<i className="fa fa-pinterest-p" aria-hidden="true"></i>
															<i className="fa fa-whatsapp" aria-hidden="true"></i>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div className="py-2">
										<div className="row">
											<div className="col-md-8 my-4 ">
												<div className="" id="slider">
													<div id="myCarousel" className="carousel slide shadow">
														<div className="carousel-inner">
															{
																car_images																	
																	?	
																		car_images.map((car_image) =>(
																			// console.log('car_image:'+car_image.image)
																			<div className={`carousel-item ${i == 0 ? 'active' : ''}`} data-slide-number={i++} >
																				<img key={car_image.id} src={car_image.image} className="img-fluid"/>
																			</div>
																		))
																		
																	:
																		''
															}
															<a className="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
																<span className="carousel-control-prev-icon" aria-hidden="true"></span>
																<span className="sr-only">Previous</span>
															</a>
															<a className="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
																<span className="carousel-control-next-icon" aria-hidden="true"></span>
																<span className="sr-only">Next</span>
															</a>

														</div>
														{/* <!-- main slider carousel nav controls --> */}


														<ul className="carousel-indicators list-inline mx-auto mt-2">
															{car_images																	
																?	
																	car_images.map((car_image) =>(
																		<li className="list-inline-item active">
																			<a id={`carousel-selector-${car_image.id}`} className={j == 0 ? 'selected' : ''} data-slide-to={j++}
																				data-target="#myCarousel">
																				<img src={car_image.image} className="img-fluid"/>
																			</a>
																		</li>
																	))
																	
																:
																	''
															}
														</ul>
													</div>
												</div>
												{/* <!--/main slider carousel--> */}


												<div className="tab-section mt-5">
													<div className="container no-gutter">
														<div className="row">
															<div className="">
																<div className="tabbable" id="tabs-211789">
																	<ul className="nav nav-tabs">
																		<li className="nav-item">
																			<a className="nav-link active" href="#tab1" data-toggle="tab"><i
																					className="fa fa-sliders" aria-hidden="true"></i>
																				Vehicle overview</a>
																		</li>
																		<li className="nav-item">
																			<a className="nav-link" href="#tab2" data-toggle="tab"><i className="fa fa-list"
																					aria-hidden="true"></i>
																				Features & Options</a>
																		</li>
																		<li className="nav-item">
																			<a className="nav-link" href="#tab3" data-toggle="tab"><i className="fa fa-list"
																					aria-hidden="true"></i>
																				Technical Specifications</a>
																		</li>
																	</ul>
																	<div className="tab-content">
																		<div className="tab-pane active" id="tab1">
																			<p className="mt-4">
																				{cardata.vehicle_overview}
																			</p>
																		</div>
																		<div className="tab-pane" id="tab2">
																			<h2>Features</h2>
																			<ul className="featurelist">
																				{
																				cardata.features_options
																					?
																					cardata.features_options.map((feature) =>(
																							// console.log('car_image:'+feature)
																							<><i class="fa fa-check-square-o" aria-hidden="true"></i> <li>{feature}</li></>
																							// <div className={`carousel-item ${i == 0 ? 'active' : ''}`} data-slide-number={i++} >
																							// 	<img key={car_image.id} src={car_image.image} className="img-fluid"/>
																							// </div>
																						))	
																					: 
																						''
																				}

																			</ul>
																		</div>
																		<div className="tab-pane" id="tab3">
																			<h2>Technical Specification</h2>
																			<table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
																				<tbody>
															
																					{
																						cardata.tech_info && cardata.tech_info.performance
																							?
																							cardata.tech_info.performance.map((performance)=>(
																								<tr className="strong">
																									<td>{performance.name}:</td>
																									<td>{performance.value}</td>
																								</tr>
																							))		
																							:
																								''
																					}
																				</tbody>
																			</table>
																			<h2>Dimension</h2>
																			<table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
																				<tbody>
															
																					{
																						cardata.tech_info && cardata.tech_info.Dimensions
																							?
																							cardata.tech_info.Dimensions.map((Dimensions)=>(
																								<tr className="strong">
																									<td>{Dimensions.name}:</td>
																									<td>{Dimensions.value}</td>
																								</tr>
																							))		
																							:
																								''
																					}
																				</tbody>
																			</table>
																		</div>
																	</div>
																</div>
															</div>


															<div className="related-vehicle mt-4">
																<h4>Related Vehicle</h4>
																<div className="row mt-5">
																	{
																		cardata.relatedcars
																			?
																				cardata.relatedcars.map((car)=>(
																					<div className="col-md-4">
																						<a className="nav-link" onClick={(ev) => {this.getVehicle(`${car.car_id}`)}}>
																							<img className="img-fluid" src={car.featured_image} alt="Card image"/>
																							<div className="card-body text-center">
																							<h4 className="card-title text-danger">{car.car_name}</h4>
																							<p className="card-text">€{car.price}</p>
																							</div>
																						</a>
																					</div>
																				))
																			:
																				''
																	}
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div className="col-md-4 mt-4">
												{
													cardata.car_info
													?
														<div className="price_section">
															<table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
																<tbody>
																	<tr className="strong">
																		<td>Price:</td>
																		<td>€{cardata.car_info.converted_price}</td>
																	</tr>
																	<tr className="strong">
																		<td>CO2 VRT:</td>
																	<td>€{cardata.car_info.co2_tax}</td>
																	</tr>
																	<tr className="strong">
																		<td>NOx VRT:</td>
																		<td>€{cardata.car_info.nox}</td>
																	</tr>
																	<tr className="strong">
																		<td>Total VRT:</td>
																		<td>€{cardata.car_info.nox+cardata.car_info.co2_tax}</td>
																	</tr>
																	<tr className="w3-border-top w3-border-bottom strong total">
																		<td class="w3-border-top">Trade&nbsp;Price:</td>
																		<td class="w3-border-top">€{cardata.car_info.final_price}</td>
																	</tr>
																</tbody>
															</table>
														</div>												
													:
														<div class="btn-block">
															<button type="button" className="btn btn-info">Call For Price</button>
												  		</div>
												}
												<div className="description mt-4">
													<h4>Description</h4>

													<div className="container mt-4">

														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Year</div>
																<div className="col-md-8">{cardata.car_year}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Make</div>
																<div className="col-md-8">{cardata.make}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Body Style</div>
																<div className="col-md-8">{cardata.body_style}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Condition</div>
																<div className="col-md-8">{cardata.car_condition}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Mileage</div>
																<div className="col-md-8">{cardata.mileage}</div>
															</div>
														</div>
														{
															cardata.vrm
															?
																<div className="row">
																	<div className="border-bottom d-md-flex w-100 py-2">
																		<div className="col-md-4">Registration Number</div>
																		<div className="col-md-8">{cardata.vrm}</div>
																	</div>
																</div>
															:
															''
														}
														{/* {
															cardata.vrm !== '' && cardata.vrm !== 0 
															?
																<div className="row">
																	<div className="border-bottom d-md-flex w-100 py-2">
																		<div className="col-md-4">Owner</div>
																		<div className="col-md-8">{cardata.owner}</div>
																	</div>
																</div>
															:
															''
														} */}
														{
															cardata.tax 
															?
																<div className="row">
																	<div className="border-bottom d-md-flex w-100 py-2">
																		<div className="col-md-4">Road Tax</div>
																		<div className="col-md-8">€{cardata.tax}</div>
																	</div>
																</div>
															:
															''
														}
														{
															cardata.car_doors 
															?
																<div className="row">
																	<div className="border-bottom d-md-flex w-100 py-2">
																		<div className="col-md-4">Doors</div>
																		<div className="col-md-8">€{cardata.car_doors}</div>
																	</div>
																</div>
															:
															''
														}
														{
															cardata.seats 
															?
																<div className="row">
																	<div className="border-bottom d-md-flex w-100 py-2">
																		<div className="col-md-4">Seats</div>
																		<div className="col-md-8">{cardata.seats}</div>
																	</div>
																</div>
															:
															''
														}
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Transmission</div>
																<div className="col-md-8">{cardata.transmission}</div>
															</div>
														</div>
														{/*<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Drivetrain</div>
																<div className="col-md-8">{cardata.drivetrain}</div>
															</div>
														</div>*/}
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Engine</div>
																<div className="col-md-8">{cardata.engine}</div>
															</div>
														</div>

														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Fuel Type</div>
																<div className="col-md-8">{cardata.fuel_type}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Fuel Economy</div>
																<div className="col-md-8">{cardata.fuel_economy}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Trim</div>
																<div className="col-md-8">{cardata.trim}</div>
															</div>
														</div>
														{/* <div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Exterior Color</div>
																<div className="col-md-8">{cardata.exterior_color}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Interior color</div>
																<div className="col-md-8">{cardata.interior_color}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">Stock Number</div>
																<div className="col-md-8">{cardata.stock_number}</div>
															</div>
														</div>
														<div className="row">
															<div className="border-bottom d-md-flex w-100 py-2">
																<div className="col-md-4">VIN Number</div>
																<div className="col-md-8">{cardata.vin_number}</div>
															</div>
														</div> */}
													</div>
												</div>
												<div className="deposit-btn">
                                        			<button type="button" className="btn btn-warning" data-toggle="modal" data-target={`#buynow`}>Place A Deposit</button>
                                        		</div>
												<div className="fuel-efficiency pt-4">
													<h4>Fuel Efficiency</h4>
													<div className="fuel-economy mt-4">
														<h2>Fuel Economy Rating</h2>
														<div className="row">
															<div className="city-info d-md-flex w-100">
																<div className="col-md-4 text-center">
																	<p><small>City</small><br/><b>20</b></p>
																</div>
																<div className="col-md-4 text-center">
																	<i className="fa fa-car" aria-hidden="true"></i>
																</div>
																<div className="col-md-4 text-center">
																	<p><small>Highway</small><br/><b>26</b></p>

																</div>
															</div>
														</div>
														<p>Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing
															industries for previewing layouts and visual mockups.</p>
													</div>
												</div>
												<div className="financing mt-4">
													<h4>Financing Calculator</h4>
													<form className="mt-4">

														<div className="form-group">
															<label for="amount">Loan Ammount*</label>
															<input type="email" className="form-control" id="" placeholder=""/>
														</div>
														<div className="form-group">
															<label for="payment">Down Payment*</label>
															<input type="password" className="form-control" id="" placeholder=""/>
														</div>
														<div className="form-group">
															<label for="inputInterest">Interest Rate(%)*</label>
															<input type="text" className="form-control" id="" placeholder=""/>
														</div>
														<div className="form-group">
															<label for="inputPeriod">Period (Month)*</label>
															<input type="text" className="form-control" id="" placeholder=""/>
														</div>

														<div className="form-group">
															<label for="inputPeriod w-100">Payment</label>

															<div className="">
																<button type="" className="btn btn-danger">Estimate Payment</button>
																<button type="" className="btn btn-danger">Clear</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									{/* Buy Vehicle */}
					                <div className="modal fade" id={`buynow`} role="dialog">
					                    <div className="login team view-order modal-dialog modal-lg">
					                        <div className="vehicle_buy modal-content">
						                            <button type="button" className="close" id="close" data-dismiss="modal">×</button>
						                        	<div className="row modal-body">
						                        		<div style={{ 'background-image': `url(${cardata.featured_image})`}} className="image_sec col-sm-6">
						                        			<h2>Buy {cardata.year} {cardata.car_name} {cardata.model}</h2>
						                        			{/*<img className="modal-img" src={cardata.featured_image} />*/}
									                    </div>
									                    <div className="form_sec col-sm-6">
									                    	<h2>Place A Deposit</h2>
							                                <Form ref={el => (this.form = el)}>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Form.Label column sm="12">
										                            	Name
										                            </Form.Label>
										                            <Col sm="12">
										                            <Form.Control type="text" name="Name" placeholder="Name" value={this.state.Name} onChange={ e => this.handleChange(e) } />
		                                    						{ this.formValidator.message('Name', this.state.Name ,'required') }								                            
										                            </Col> 
										                        </Form.Group>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Form.Label column sm="12">
										                            	Email
										                            </Form.Label>
										                            <Col sm="12">
										                            <Form.Control type="text" name="Email" placeholder="Email" value={this.state.Email} onChange={ e => this.handleChange(e) } />
		                                    						{ this.formValidator.message('Email', this.state.Email ,'required|email') }								                            
										                            </Col> 
										                        </Form.Group>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Form.Label column sm="12">
										                            	Phone
										                            </Form.Label>
										                            <Col sm="12">
										                            <PhoneInput 
				                                                    placeholder="Enter phone number" 
				                                                    defaultCountry="GB"
				                                                    value={this.state.Phone}
				                                                    className="form-input"
				                                                    onChange={e => this.setphone(e)} />
										                            {/*<Form.Control type="text" name="Phone" placeholder="Phone" value={this.state.Phone} onChange={ e => this.handleChange(e) } />*/}
		                                    						{ this.formValidator.message('Phone', this.state.Phone ,'required|phone') }								                            
										                            </Col> 
										                        </Form.Group>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Form.Label column sm="12">
										                            	12 months Warranty
										                            </Form.Label>
										                            <Col sm="12">
										                                <Form.Control size="sm" as="select" name="twelvemonthwarrenty" onChange={ e => this.handleChange(e) } >
										                                    <option value="">Select</option>
										                                    <option value="premiummax395">Premium Max (+€395)</option>
										                                    <option value="premiumplus395">Premium Plus (+€395)</option>
										                                    <option value="premiumpowertrain295">Premium Component (+€395)</option>
										                                    <option value="premiumcomp395">Premium Power Train (+€295)</option>
										                                </Form.Control>
		                                    							{ this.formValidator.message('12 months Warranty', this.state.twelvemonthwarrenty ,'required') }								                            
										                            </Col>
									                        	</Form.Group>
									                        	<Form.Group as={Row} controlId="formPlaintextName">
										                            <Col sm="12">
										                            <Form.Control className="check-box-style" type="checkbox" name="vrt_proccessing" placeholder="VRT Processing" value={this.state.vrt_proccessing} onChange={ e => this.handleCheckbox(e) } /> VRT Processing (+€295)
										                            </Col> 
										                        </Form.Group>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Col sm="12">
										                            <Form.Control className="check-box-style" type="checkbox" name="transferuktodub" placeholder="Transport UK to Dub" value={this.state.transferuktodub} onChange={ e => this.handleCheckbox(e) } /> Transport UK to Dub (+€395)
										                            </Col> 
										                        </Form.Group>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Col sm="12">
										                            <Form.Control className="check-box-style" type="checkbox" name="homedelivry" placeholder="Home Delivery" value={this.state.homedelivry} onChange={ e => this.handleCheckbox(e) } /> Home Delivery (+€175)
										                            </Col> 
										                        </Form.Group>
										                        <Form.Group as={Row} controlId="formPlaintextName">
										                            <Col sm="12">
										                            <Form.Control className="check-box-style" type="checkbox" name="tnc" placeholder="Terms and Conditions" value={this.state.tnc} onChange={ e => this.handleCheckbox(e) } /> I accept Terms and Conditions and Privacy Policy
		                                    						{ this.formValidator.message('Terms and Conditions', this.state.tnc ,'required') }								                            
										                            </Col> 
										                        </Form.Group>
										                        <Button variant="primary" className="text-right" type="button" onClick={ e => this.submitform(e) } disabled={ this.state.isSubmit } >{this.state.isSubmit ? 'Please wait..' : 'Submit'}</Button>
										                    </Form>
									                    </div>
					                            	</div>
												
					                        </div>
					                    </div>
					                </div> 
								</>
                            : 
                            	''
                            
                }
           	</>
        );
    }
}

const mapStateToProps = (state) => ({
    cardata: state.car.cardata,
    carloading: state.car.carloading
})
export default connect(mapStateToProps)(SingleVehicle);

// export default SingleVehicle;