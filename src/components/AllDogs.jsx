import React, { Component } from 'react';
import {Collapse} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import { getDoglisting, getDogsFilter, getFilteredDogs } from '../store/actions/DogActions';
import { connect } from 'react-redux';

class AllDogs extends Component {
    constructor(props){
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            filter: false,
            Location :  [],
            Breeds: [],
            litter: [],
            sex: [],
            Dog_color: [],
            size: [],
            age: [],
            dogprice: [],
            Locationtoggle : false,
            Pricetoggle : false,
            Breedtoggle : false,
            Agetoggle : false,
            Sextoggle : false,
            Littretoggle : false,
            Colortoggle : false,
            Sizetoggle : false,
            all_dogs:'',
            all_dogs_filter:''
        }
    }
    handleChange = (e) => {
        this.setState({ filter: true });
        let isChecked = e.target.checked;
        let checkname = [e.target.name];
        let values = this.state[checkname]
        if(isChecked){
            values.push(e.target.value);
        }else{
            var index = values.indexOf(e.target.value)
            values.splice(index, 1);
        }
        this.setState({
            checkname: JSON.stringify(values)
        })
        //console.log(checkname+' : '+ this.state[checkname]);
        const {Location,Breeds,litter,sex,Dog_color,size,age,dogprice} = this.state
        if(Location.length !== 0){
            var locat = Location.toString();
            //console.log('locat :'+ Location.toString());
        }else{
            var locat = '';
        }
        if(dogprice.length !== 0){
            var dogpric = dogprice.toString();;
        }else{
            var dogpric = '';
        }
        if(Breeds.length !== 0){
            var Breed = Breeds.toString();
        }else{
            var Breed = '';
        }
        if(age.length !== 0){
            var dogage = age.toString();
        }else{
            var dogage = '';
        }
        if(sex.length !== 0){
            var dogsex = sex.toString();
        }else{
            var dogsex = '';
        }
        if(litter.length !== 0){
            var doglitter = litter.toString();
        }else{
            var doglitter = '';
        }
        if(Dog_color.length !== 0){
            var dogcolor = Dog_color.toString();
        }else{
            var dogcolor = '';
        }
        if(size.length !== 0){
            var dogsize = size.toString();
        }else{
            var dogsize = '';
        }
        this.props.dispatch(getFilteredDogs(locat,dogpric,Breed,dogage,dogsex,doglitter,dogcolor,dogsize));
    }

    componentDidMount() {
        this.props.dispatch(getDoglisting());
        this.props.dispatch(getDogsFilter());
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.filter) {
            this.setState({
                all_dogs: nextProps.filtereddogsdata
            });
        } else {
            this.setState({
                all_dogs: nextProps.alldogs
            });
        }
        this.setState({
            all_dogs_filter: nextProps.dogfilters,
        });

        //console.log('nextProps.dogfilters'+JSON.stringify(nextProps.filtereddogsdata));
    }

    toggle(toggler){
        let togglerStatus = this.state[toggler];
        // console.log(toggler);
        this.setState({
        [toggler]: !togglerStatus // change the status only for the toggle you clicked
        });
    }
    
    render() {
        const {all_dogs_filter} = this.state
        return (
            <div className="Dogs-listing-sec">
                <div className="container">
                        <div className="row">
                            <div className="col-sm-3">
                                <div className="row p-5">
                                    <div className="filter-sec">
                                        <h2 className="title">Filter by</h2>
                                    
                                    {
                                        Object.keys(all_dogs_filter).map((val, i) => {
                                            if(val == 'Location'){
                                                const locations = all_dogs_filter[val];
                                                return (
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Locationtoggle ? "active" : ""}`} onClick={() => this.toggle('Locationtoggle')}>Location</li>
                                                        <Collapse in={this.state.Locationtoggle}>
                                                            <div className="collapse-content">
                                                                {
                                                                locations.map((post) => (
                                                                    <><span className="item"><input type="checkbox" name={val} value={post.name} onChange={e => this.handleChange(e)} /><span>{post.name}</span></span></>
                                                                ))
                                                                }
                                                            </div>
                                                        </Collapse>
                                                    </>
                                                )
                                            }else if(val == 'Breeds'){
                                                const Breeds = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Breedtoggle ? "active" : ""}`} onClick={() => this.toggle('Breedtoggle')}>Breed</li>
                                                        <Collapse in={this.state.Breedtoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        Breeds.map((post) => (
                                                                            <><span className="item"><input type="checkbox" name={val} value={post.name} onChange={e => this.handleChange(e)} /><span>{post.name}</span></span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse> 
                                                    </>
                                                )
                                                
                                            }else if(val == 'litter'){
                                                const litter = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Littretoggle ? "active" : ""}`} onClick={() => this.toggle('Littretoggle')}>Littre</li>
                                                        <Collapse in={this.state.Littretoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        litter.map((post) => (
                                                                            <><span className="item"><input type="checkbox" name={val} value={post.value} onChange={e => this.handleChange(e)} /><span>{post.value}</span></span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse>  
                                                    </>
                                                )
                                            }else if(val == 'sex'){
                                                const sex = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Sextoggle ? "active" : ""}`} onClick={() => this.toggle('Sextoggle')}>Sex</li>
                                                        <Collapse in={this.state.Sextoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        sex.map((post) => (
                                                                            <><span className="item"><input type="checkbox" name={val} value={post.name} onChange={e => this.handleChange(e)} /><span>{post.name}</span></span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse>  
                                                    </>
                                                )
                                            }else if(val == 'Dog_color'){
                                                const Dog_color = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Colortoggle ? "active" : ""}`} onClick={() => this.toggle('Colortoggle')}>Color</li>
                                                        <Collapse in={this.state.Colortoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        Dog_color.map((post) => (
                                                                            <><span className="item"><input type="checkbox" name={val} value={post.color} onChange={e => this.handleChange(e)} /><span>{post.color}</span></span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse>
                                                    </>
                                                )
                                            }else if(val == 'size'){
                                                const size = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Sizetoggle ? "active" : ""}`} onClick={() => this.toggle('Sizetoggle')}>Size</li>
                                                        <Collapse in={this.state.Sizetoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        size.map((post) => (
                                                                            <><span className="item"><input type="checkbox" name={val} value={post.value} onChange={e => this.handleChange(e)} /><span>{post.size}</span></span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse>
                                                    </>
                                                )
                                            }else if(val == 'age'){
                                                const age = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Agetoggle ? "active" : ""}`} onClick={() => this.toggle('Agetoggle')}>Age</li>
                                                        <Collapse in={this.state.Agetoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        age.map((post) => (
                                                                            <><span className="item"><input type="checkbox" name={val} value={post.value} onChange={e => this.handleChange(e)} /><span>{post.age}</span></span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse>  
                                                    </>
                                                )
                                            }else if(val == 'dogprice'){
                                                const dogprice = all_dogs_filter[val];
                                                return(
                                                    <>
                                                        <li className={`filter-list-item ${this.state.Pricetoggle ? "active" : ""}`} onClick={() => this.toggle('Pricetoggle')}>Price</li>
                                                        <Collapse in={this.state.Pricetoggle}>
                                                            <div className="collapse-content">
                                                                    {
                                                                        dogprice.map((post) => (
                                                                            <><span className="item"></span><input type="checkbox" value={post.value} name={val} onChange={e => this.handleChange(e)} /><span>{post.size}</span></>
                                                                        ))
                                                                    }
                                                            </div>
                                                        </Collapse>
                                                    </>
                                                )
                                            }
                                        })
                                    }
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-9">
                                <div className="row p-5">
                                    {
                                          
                                        this.props.alldogloading?
                                        <div className="alltourticksloader">
                                            <h1>Loadingg...............</h1> 
                                        </div>
                                        :
                                        this.state.all_dogs?this.state.all_dogs.length ? this.state.all_dogs.map((post) => (
                                        <div className="col-sm-6 dog-list-item p-1">
                                            <div className="item">
                                                <div className="image-sec">
                                                    <div className='dog-image'>
                                                        <img src={post.featured_image} alt=""/>
                                                    </div>
                                                </div>
                                                <div className="dog-details">
                                                    <h4 className="title">{post.breed}</h4>
                                                    <p className="dog-sex">{post.gender}</p>
                                                    <p className="dog-age">{post.age}</p>
                                                    <p className="dog-location">{post.location}</p>
                                                </div>
                                                <div className="actions">
                                                    <p className="dog-price">${post.price}</p>
                                                    <a className="dog-details" href="#modal-container-dogdetail" role="button" data-toggle="modal">View Details</a>
                                                    <p className="dog-buy-action"><button type="button" className="buy-btn">Buy Now</button></p>
                                                </div>
                                            </div>
                                        </div>
                                        ))
                                        :<div>No Dogs</div>:<div>No Dogs</div>
                                    }                                                        
                              </div>
                            </div>
                        </div>
                    </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    alldogs: state.dogs.alldogs,
    alldogloading: state.dogs.loading,
    dogfilters: state.dogs.alldogsfilter,
    dogfiltersloading: state.dogs.filterloading,
    filtereddogsdata: state.dogs.filtereddogs
})
export default connect(mapStateToProps)(AllDogs);