import React, { Component } from 'react';

class HomePage extends Component {
    render() {
        return (
			<>
              <section class="banner-lets-find">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5 offset-md-7">
          <div class="lets-find-car">
            <h1 class="text-uppercase">Let's Find<br/><span>A Car</span><br/><span class="for">For You</span></h1>
            <a href="#" class="btn btn-info text-uppercase">View Cars</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="banner-whoweare">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="let-side-cont">
            <h2>Who we are</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente.</p>
            <a href="#" class="btn btn-info text-uppercase">Rad More</a>
          </div>
        </div>
        <div class="col-md-6">
          <div class="right-side-image">
            <img src="assets/images/img2.jpg" alt="computer-image"class="img-fluid w-100"/>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="whatwe-do">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center title-head">
            <h2>What we do</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="work-idea">
            <div class="img-box">
              <img src="assets/images/flag.png" alt="round-image" class=""/>
            </div>
            <div class="cont-box">
              <h4>Creative Work</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-idea">
            <div class="img-box">
              <img src="assets/images/computer.png" alt="round-image" class=""/>
            </div>
            <div class="cont-box">
              <h4>We do Best</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-idea">
            <div class="img-box">
              <img src="assets/images/arrows.png" alt="round-image" class=""/>
            </div>
            <div class="cont-box">
              <h4>Best Company</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-5">
        <div class="col-md-4">
          <div class="work-idea">
            <div class="img-box">
              <img src="assets/images/pic.png" alt="round-image" class=""/>
            </div>
            <div class="cont-box">
              <h4>Creative Work</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-idea">
            <div class="img-box">
              <img src="assets/images/arrow-pic.png" alt="round-image" class=""/>
            </div>
            <div class="cont-box">
              <h4>We do Best</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-idea">
            <div class="img-box">
              <img src="assets/images/msg-icon.png" alt="round-image" class=""/>
            </div>
            <div class="cont-box">
              <h4>Best Company</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="how-to-work">
    <div class="container">
      <div class="row">
        <div class="col-md-12 title-head">
          <h2>How it works</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="let-side-cont">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente.</p>
            <a href="#" class="btn btn-info text-uppercase">Rad More</a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="right-side-image">
            <img src="assets/images/car-1.png" alt="computer-image"class="img-fluid w-100"/>
          </div>
        </div>
        <div class="col-md-3">
          <div class="right-side-image">
            <img src="assets/images/car-2.png" alt="computer-image"class="img-fluid w-100"/>
          </div>
        </div>
        <div class="col-md-3">
          <div class="right-side-image">
            <img src="assets/images/hand-shake.png" alt="computer-image"class="img-fluid w-100"/>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="our-team">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="text-center">
<h2 class="mb-5">Our team</h2>
</div>
</div>
</div>
</div>
<div class="container-fluid p-0">
<div class="team-group">
<div class="img-box">
<img src="assets/images/img1.jpg" alt="team-member" class=""/>
<p>Jhon Doe</p>
</div>

<div class="img-box">
<img src="assets/images/img1.jpg" alt="team-member" class=""/>
<p>Richard</p>
{/* <!-- <span>Sales Head</span> --> */}
</div>

<div class="img-box">
<img src="assets/images/img1.jpg" alt="team-member" class=""/>
<p>Jhon Doe</p>
</div>



</div>
</div>
</section>
    {/* <section class="our-team">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <h2 class="mb-5">Our team</h2>                     
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid p-0 overflow-hidden">
      <div class="center slide">
        <div class="img-box">
            <img src="assets/images/img1.jpg" alt="team-member" class=""/>
           <p>Jhon Doe</p>
        </div>

        <div class="img-box">
            <img src="assets/images/img1.jpg" alt="team-member" class=""/>
           <p>Richard</p>
        </div>

        <div class="img-box">
            <img src="assets/images/img1.jpg" alt="team-member" class=""/>
           <p>Jhon Doe</p>
        </div>

        <div class="img-box">
            <img src="assets/images/img1.jpg" alt="team-member" class=""/>
           <p>Jhon Doe</p>
        </div>

      </div>
    </div>
  </section> */}

  <section class="our-team" id="customer-sayabout">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title-head">
            <h2 class="mb-4">What customer says</h2>                     
          </div>
        </div>
      </div>
    </div>
    <div class="container p-0 overflow-hidden">
      <div class="row"><div class="col-md-12">
        <div id="carousel-reviews" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="col-md-12">
                <div class="main-box">
                  <div class="image-box">
                    <img src="assets/images/img2.jpg" alt="image" class="img-fuid"/>
                  </div>
                  <div class="cont-box">
                    <p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <span class="c-name">Jinny Snow-BMW M5</span>
                    <div class="customer-pic">
                      <div class="img-boxes">
                        <a href="#"><img src="assets/images/img2.jpg" alt="image" class="img-fuid"/></a>
                      </div>
                      <div class="img-boxes">
                        <a href="#"><img src="assets/images/img1.jpg" alt="image" class="img-fuid"/></a>
                      </div>
                      <div class="img-boxes">
                        <a href="#"><img src="assets/images/img2.jpg" alt="image" class="img-fuid"/></a>
                      </div>
                    </div>
                  </div>                 
                </div>
              </div>
            </div>

            <div class="carousel-item item">
              <div class="col-md-12">
                <div class="main-box">
                  <div class="image-box">
                    <img src="assets/images/img1.jpg" alt="image" class="img-fuid"/>
                  </div>
                  <div class="cont-box">
                    <p><i class="fa fa-quote-left"></i>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <span class="c-name">Jinny Snow-BMW M5</span>
                    <div class="customer-pic">
                      <div class="img-boxes">
                        <a href="#"><img src="assets/images/img2.jpg" alt="image" class="img-fuid"/></a>
                      </div>
                      <div class="img-boxes">
                        <a href="#"><img src="assets/images/img1.jpg" alt="image" class="img-fuid"/></a>
                      </div>
                      <div class="img-boxes">
                        <a href="#"><img src="assets/images/img2.jpg" alt="image" class="img-fuid"/></a>
                      </div>
                    </div>
                  </div>
                                   
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div>
  </section>
            </>
        );
    }
}


export default HomePage;