import { apiBaseUrl, callHandleArray } from '../helpers/common';
import * as actionTypes from '../types/types';
import {handleResponse} from '../helpers/userServices';

export const fetchLeadsStart = () => {
    return{
        type: actionTypes.FETCH_COMMON_LEADS_BEGIN
    }
}
export const fetchLeadsSuccess = (data) => {
    return{
        type: actionTypes.FETCH_COMMON_LEADS_SUCCESS,
        payload: data.data

    }
}
export const fetchCommonStatesStart = () => {
    return{
        type: actionTypes.FETCH_COMMON_STATES_BEGIN
    }
}
export const fetchCommonStatesSuccess = (data) => {
    return{
        type: actionTypes.FETCH_COMMON_STATES_SUCCESS,
        payload: data.Data

    }
}
export const fetchCommonPostsStart = () => {
    return{
        type: actionTypes.FETCH_COMMON_POSTS_BEGIN
    }
}
export const fetchCommonPostsSuccess = (data) => {
    return{
        type: actionTypes.FETCH_COMMON_POSTS_SUCCESS,
        payload: data.data

    }
}
export const fetchCommonReviewsStart = () => {
    return {
        type: actionTypes.FETCH_COMMON_REVIEWS_BEGIN
    }
}
export const fetchCommonReviewsSuccess = (data) => {
    return {
        type: actionTypes.FETCH_COMMON_REVIEWS_SUCCESS,
        payload: data.data
    }
}
export const fetchCommonSettingsStart = () => {
    return {
        type: actionTypes.FETCH_COMMON_SETTINGS_BEGIN
    }
}
export const fetchCommonSettingsSuccess = (data) => {
    return {
        type: actionTypes.FETCH_COMMON_SETTINGS_SUCCESS,
        payload: data.data
    }
}

export function getsettings(){
    return dispatch => {
        dispatch(fetchCommonSettingsStart());
        const request = new Request(`${apiBaseUrl}/get-settings`,{
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json' }),
        });
        return fetch(request).then(handleResponse).then((data) => {
            // console.log('settings'+JSON.stringify(data))
            dispatch(fetchCommonSettingsSuccess(data));
        })
    }

}

export function getstates(){
    return dispatch => {
        dispatch(fetchCommonStatesStart());
        const request = new Request(`${apiBaseUrl}/get-states`,{
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json' }),
        });
        return fetch(request).then(handleResponse).then((data) => {
            // console.log('statesdata'+JSON.stringify(data))
            dispatch(fetchCommonStatesSuccess(data));
        })
    }

}

export function getLeads(){
    return dispatch => {
        dispatch(fetchLeadsStart());
        const request = new Request(`${apiBaseUrl}/user/get-leads`,{
            method: 'GET',
            headers: new Headers({ 'X-Auth-Token': `${localStorage.getItem('token')}` }),
        });
        return fetch(request).then(handleResponse).then((data) => {
            // console.log('Leadsdata'+JSON.stringify(data))
            dispatch(fetchLeadsSuccess(data));
        })
    }
}

export function getPosts(breeder_id){
    return dispatch => {
        dispatch(fetchCommonPostsStart());
        const request = new Request(`${apiBaseUrl}/user/get-breeder-posts/`+breeder_id,{
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'X-Auth-Token': `${localStorage.getItem('token')}` }),
        });
        return fetch(request).then(handleResponse).then((data) => {
            // console.log('POSTSdata'+JSON.stringify(data))
            dispatch(fetchCommonPostsSuccess(data));
        })
    }
}

export function getReviews(breeder_id){
    return dispatch => {
        dispatch(fetchCommonReviewsStart());
        const request = new Request(`${apiBaseUrl}/user/get-breeder-reviews/`+breeder_id,{
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'X-Auth-Token': `${localStorage.getItem('token')}` }),
        });
        return fetch(request).then(handleResponse).then((data) => {
            // console.log('POSTSdata'+JSON.stringify(data))
            dispatch(fetchCommonReviewsSuccess(data));
        })
    }
}