import { apiBaseUrl, callHandleArray } from '../helpers/common';
import * as actionTypes from '../types/types';
import {handleResponse} from '../helpers/userServices';



export const fetchStart = () => {
    return {
        type: actionTypes.FETCH_USER_DATA_BEGIN
    }
}
export const fetchUserSuccess = (data) => {
    return {
        type: actionTypes.FETCH_USER_DATA_SUCCESS,
        payload: data.data
    }
}
export const fetchBreederStart = () => {
    return {
        type: actionTypes.FETCH_BREEDER_DATA_BEGIN
    }
}
export const fetchBreederSuccess = (data) => {
    return {
        type: actionTypes.FETCH_BREEDER_DATA_SUCCESS,
        payload: data.data
    }
}
export function getUserdetails(id) {    
    return dispatch => {
        dispatch(fetchStart());
        const request = new Request(`${apiBaseUrl}/user/get-user/${id}`, {
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'X-Auth-Token': `${localStorage.getItem('token')}` }),
        });

        return fetch(request).then(handleResponse).then((data) => {
            // console.log('data :'+JSON.stringify(data));
            dispatch(fetchUserSuccess(data));
        });
    }
}

export function getbreederdetails(breeder_id){
    return dispatch => {
        dispatch(fetchBreederStart());
        const request = new Request(`${apiBaseUrl}/user/get-breeder/${breeder_id}`, {
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'X-Auth-Token': `${localStorage.getItem('token')}` }),
        });

        return fetch(request).then(handleResponse).then((data) => {
            //console.log('data :'+JSON.stringify(data));
            dispatch(fetchBreederSuccess(data));
        });
    }
}