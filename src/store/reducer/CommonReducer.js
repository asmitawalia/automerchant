import * as types from "../types/types";

const initalState = {
    statesloading: true,   
    states: [],
    postsloading: true,
    posts: [],
    reviewloading: true,
    reviewsdata: [],
    leadsloading: true,
    leadsdata: [],
    settings:[],
    settingsloading:true
}

const commonReducer = (state = initalState, action) => {
    switch (action.type) {
        case types.FETCH_COMMON_LEADS_BEGIN:
            return {
                ...state,
                leadsloading: true
            }
        case types.FETCH_COMMON_LEADS_SUCCESS:
            // console.log('action.payload'+action.payload)
            return {
                ...state,
                leadsloading: false,
                leadsdata: action.payload
            }
        case types.FETCH_COMMON_STATES_BEGIN:
            return {
                ...state,
                statesloading: true
            }
        case types.FETCH_COMMON_STATES_SUCCESS:
            // console.log('action.payload'+action.payload)
            return {
                ...state,
                statesloading: false,
                states: action.payload
            }
        case types.FETCH_COMMON_POSTS_BEGIN:
            return {
                ...state,
                postsloading: true
            }
        case types.FETCH_COMMON_POSTS_SUCCESS:
            return {
                ...state,
                postsloading: false,
                posts: action.payload
            }
        case types.FETCH_COMMON_REVIEWS_BEGIN:
            return {
                ...state,
                reviewloading: true
            }
        case types.FETCH_COMMON_REVIEWS_SUCCESS:
            return {
                ...state,
                reviewloading: false,
                reviewsdata: action.payload
            }
        case types.FETCH_COMMON_SETTINGS_BEGIN:
            return {
                ...state,
                settingsloading: true
            }
        case types.FETCH_COMMON_SETTINGS_SUCCESS:
            return {
                ...state,
                settingsloading: false,
                settings: action.payload
            }
        default:
            return state
    }
}
export default commonReducer;