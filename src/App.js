import React, { Component } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import ScrollToTop from 'react-router-scroll-top';
import { history } from './store/helpers/history';
import EmptyLayout from './layout/EmptyLayout';
import AdminLayout from './layout/AdminLayout';
import InnerLayout from './layout/InnerLayout';
import { RouteWithLayout } from './layout/RouteWithLayout';
import VehicleList from './components/VehicleList'
import VehicleListnew from './components/VehicleListnew'
import HomePage from './components/HomePage';
import About from './components/About';
import Contact from './components/Contact';
import SingleVehicle from './components/SingleVehicle';
import Registration from './components/Registration';
import HttpsRedirect from 'react-https-redirect';
import Dashboard from './admincomponents/Dashboard';
import AdminVehicleList from './admincomponents/AdminVehicleList';
import AdminVehicleListnew from './admincomponents/AdminVehicleListnew';
import AdminLeadList from './admincomponents/AdminLeadList';
import AdminSettings from './admincomponents/AdminSettings';
import AdminSingleVehicle from './admincomponents/AdminSingleVehicle';
import SignIn from './components/SignIn';
class App extends Component {
  render() {
    return (
      <HttpsRedirect>
      <BrowserRouter history={history} >
      <ScrollToTop>
          {
            <Switch>
                <RouteWithLayout exact={true} layout={EmptyLayout} path="/" component={HomePage} />
                <RouteWithLayout layout={EmptyLayout} path="/about" component={About} />
                <RouteWithLayout layout={EmptyLayout} path="/contact" component={Contact} />
                <RouteWithLayout layout={EmptyLayout} path="/single-vehicle/:id" component={SingleVehicle} />
                <RouteWithLayout layout={EmptyLayout} path="/registration" component={Registration} />
                <RouteWithLayout layout={EmptyLayout} path="/vehicle-lists" component={VehicleList} />
                <RouteWithLayout layout={EmptyLayout} path="/vehicle-lists-new" component={VehicleListnew} />
                <RouteWithLayout layout={EmptyLayout} path="/sign-in" component={SignIn} />
                <RouteWithLayout layout={AdminLayout} path="/vehicles" component={AdminVehicleList} />
                <RouteWithLayout layout={AdminLayout} path="/vehiclesnew" component={AdminVehicleListnew} />
                <RouteWithLayout layout={AdminLayout} path="/leads" component={AdminLeadList} />
                <RouteWithLayout layout={AdminLayout} path="/settings" component={AdminSettings} />
                <RouteWithLayout layout={AdminLayout} path="/vehicle/:id" component={AdminSingleVehicle} />
                <RouteWithLayout layout={AdminLayout} path="/dashboard" component={Dashboard} />                
            </Switch>
          }
          </ScrollToTop>
      </BrowserRouter>
      </HttpsRedirect>
    );
  }
}
export default App;