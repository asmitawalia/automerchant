import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getLeads } from '../store/actions/CommonActions';
import { NavLink, Redirect } from 'react-router-dom';

class AdminLeadList extends Component {

	constructor(props){
        super(props);
        this.state = {
        	leadsdata:'',
        }
    }
	componentDidMount() {
        this.props.dispatch(getLeads());
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            leadsdata: nextProps.leadsdata,
        });
        // console.log('leadsdatasssssssss :'+JSON.stringify(nextProps.leadsdata));
    }
	render(){
		const {leadsdata} = this.state;
		return(
				<> 	
					<div className="row wrapper border-bottom white-bg page-heading">
			            <div className="col-lg-10">
			                <h2>Leads</h2>
			                <ol className="breadcrumb">
			                    <li className="breadcrumb-item">
			                        <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
			                    </li>
			                    <li className="breadcrumb-item active">
			                        <NavLink className="nav-link" to="/leads">Leads</NavLink>
			                    </li>
			                </ol>
			            </div>
			            <div className="col-lg-2">

			            </div>
			        </div>
			        <div className="wrapper wrapper-content animated fadeInRight">
			            <div className="row">
			                <div className="col-lg-12">
			                <div className="ibox ">
			                    <div className="ibox-title">
			                        <h5>List of Leads</h5>
			                        <div className="ibox-tools">
			                            
			                        </div>
			                    </div>
			                    <div className="ibox-content">
				                    <div className="table-responsive">
					                    <table className="table table-striped table-bordered table-hover vehicle-list" >
						                    <thead>
							                    <tr>
							                        <th>Name</th>
							                        <th>Email</th>
							                        <th>Phone</th>
													<th>12 month Warranty</th>
													<th>VRT Processing</th>
													<th>Transport UK to Dub</th>
													<th>Home Delivery</th>
							                        <th>Car Link</th>
							                    </tr>
						                    </thead>
						                    <tbody>
						                  	  	{ 
								  					this.props.leadsloading 
									  					? 	
									  						<div className="alltourticksloader">
									                        	<img className="loader_img" src="/assets/images/straight-loader.gif" />
									                    	</div>
								                        : leadsdata
								                        	? leadsdata.length 
							                                    ? 
							                                        leadsdata.map((post) => (
							                                        	<tr>
							                                        		<td>{post.Name}</td>
							                                        		<td>{post.Email}</td>
							                                        		<td>{post.Phone}</td>
							                                        		<td>
																				{
																					post.twelvemonthwarrenty
																						?
																							post.twelvemonthwarrenty		
																						:
																							''
																				}
																			</td>
							                                        		<td>
																				{
																					post.vrt_proccessing && post.vrt_proccessing === '1'
																						?
																							'Yes'		
																						:
																							'No'
																				}
																			</td>
							                                        		<td>
																				{
																					post.transferuktodub && post.transferuktodub === '1'
																						?
																							'Yes'		
																						:
																							'No'
																				}
																			</td>
							                                        		<td>
																				{
																					post.homedelivry && post.homedelivry === '1'
																						?
																							'Yes'		
																						:
																							'No'
																				}
																			</td>
																			<td><a target="_blank" href={`https://www.autotrader.co.uk/car-details/${post.car_id}`}>Autotrader Link</a><br></br><a target="_blank" href={`https://automerchant.ie/single-vehicle/${post.car_id}`}>Automerchant Link</a></td>
							                                        	</tr>						                                        					
							                                        ))
							                                    : <div>No Leads</div>
							                                : <div>No Leads</div>
								  				}


						                    </tbody>
						                    <tfoot>
							                    <tr>
							                        <th>Name</th>
							                        <th>Email</th>
							                        <th>Phone</th>
							                        <th>Car Link</th>
													<th>12 month Warranty</th>
													<th>VRT Processing</th>
													<th>Transport UK to Dub</th>
													<th>Home Delivery</th>
							                    </tr>
						                    </tfoot>
					                    </table>
				                    </div>
			                    </div>
			                </div>
			            </div>
			            </div>
			        </div>
			    </>
			)
	}
}

const mapStateToProps = (state) => ({
    leadsdata: state.common.leadsdata,
    leadsloading: state.common.leadsloading
})
export default connect(mapStateToProps)(AdminLeadList);