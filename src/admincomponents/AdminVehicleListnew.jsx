import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { getcarslistingnew,getFilteredCarsnew,getfiltersnew } from '../store/actions/CarActions';
import { getsettings } from '../store/actions/CommonActions';
import SimpleReactValidator from 'simple-react-validator';
import {Button, Form, Col, Row} from 'react-bootstrap';
import { apiBaseUrl } from './../store/helpers/common';
import toastr from "reactjs-toastr";
import { NavLink } from 'react-router-dom';
import PhoneInput from 'react-phone-number-input';
import $ from "jquery";
import { connect } from 'react-redux';
import Pagination from "react-js-pagination";
class AdminVehicleListnew extends Component {
	constructor(props){
        super(props);
        this.formValidator =  new SimpleReactValidator();
        this.state = {
        	carsdata:[],
        	carimg: '',
        	carname: '',
        	car_id:'',
			Name:'',
			Email:'',
            pageOfItems: [],
			Phone:'',
			twelvemonthwarrenty:'',
			premiummax395:'',
			vrt_proccessing:"0",
			transferuktodub:"0",
			homedelivry:"0",
			tnc:'',
			Year:'',
			Make:'',
			Model:'',
			body_style:'',
			engine:'',
			color:'',
			Condition:'',
			Mileage:'',
			transmission_type:'',
			Fuel:'',
			filter:false,
            filterdata: [],
            filtermodeldata: [],
            filterbodystyledata: [],
            carsloader: true,
            total_cars:'',
			activePage: 1,
			total_pages: '',
            isSubmit: false,
            settings:''
        }
    }
	handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    onfilterChange = (ev) => {
    	this.setState({activePage:''});
    	if(ev.target.name === 'Year'){
    		var Year = ev.target.value;
    	}else{
    		var Year = this.state.Year;
    	} 

    	if(ev.target.name === 'Make'){
    		var Make = ev.target.value;
    		// this.props.dispatch(getfiltermodel(ev.target.value));
    	}else{
    		var Make = this.state.Make;
    	}
    	
    	if(ev.target.name === 'Model'){
    		var Model = ev.target.value;
    		// this.props.dispatch(getfilterbodystyle(ev.target.value));
    	}else{
    		var Model = this.state.Model;
    	}

    	if(ev.target.name === 'Fuel'){
    		var Fuel = ev.target.value;
    	}else{
    		var Fuel = this.state.Fuel;
    	}

    	if(ev.target.name === 'body_style'){
    		var body_style = ev.target.value;
    	}else{
    		var body_style = this.state.body_style;    		
    	}

    	if(ev.target.name === 'Condition'){
    		var Condition = ev.target.value;
    	}else{
    		var Condition = this.state.Condition;
    	}

    	if(ev.target.name === 'transmission_type'){
    		var transmission_type = ev.target.value;
    	}else{
    		var transmission_type = this.state.transmission_type;
    	}

    	if(ev.target.name === 'Mileage'){
    		var Mileage = ev.target.value;
    	}else{
    		var Mileage = this.state.Mileage;
    	}

    	if(ev.target.name === 'engine'){
    		var engine = ev.target.value;
    	}else{
    		var engine = this.state.engine;
    	}

    	if(ev.target.name === 'color'){
    		var color = ev.target.value;
    	}else{    		
    		var color = this.state.color;
    	}
    	this.setState({ [ev.target.name]: ev.target.value, filter: true });        
    	var pagenum = 0;
        //const {Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color} = this.state;
        this.props.dispatch(getfiltersnew(Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color));
        this.props.dispatch(getFilteredCarsnew(Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,pagenum));
    }
    resetFilters = () => {
    	this.setState({ filter: false });
        this.props.dispatch(getcarslistingnew(0));
    	this.setState({Year:'',Make:'',Model:'',Fuel:'',body_style:'',Condition:'',Mileage:'',transmission_type:'',engine:'',color:''});
		const {Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color} = '';
        this.props.dispatch(getfiltersnew(Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color));		
    }
    handlePageChange(pageNumber) {
	    this.setState({activePage: pageNumber});
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        if(this.state.filter == true){
        	var pagenum = (pageNumber-1);
        	const { Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine } = this.state
        	this.props.dispatch(getFilteredCarsnew(Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,pagenum));
        }else{
	    	this.props.dispatch(getcarslistingnew(pageNumber-1));        	
        }
	}
    handleCheckbox = (e) => {
    	let value_checked = "0";
    	if(e.target.checked == true){
    		value_checked = "1";
    	}else if(e.target.name == 'tnc'){
    		value_checked = '';
    	}
    	this.setState({
            [e.target.name]: value_checked
        });
    }
	componentDidMount() {
        const {Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color} = this.state;
        this.props.dispatch(getsettings());
        this.props.dispatch(getcarslistingnew(0));
        this.props.dispatch(getfiltersnew(Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color));
    }

    componentWillReceiveProps(nextProps) {
    	if(this.state.filter){
    		// console.log(nextProps.filteredcardata)
    		this.setState({
	            carsdata: nextProps.filteredcardata.cars,
	            total_cars: nextProps.filteredcardata.count,
	            carsloader: nextProps.filteredcarloading,
	        });
    	}else{
    		this.setState({
	            carsdata: nextProps.carsdata.cars,
    			total_cars: nextProps.carsdata.count,
	            carsloader: nextProps.carsloading,
	        });
    	}
    	this.setState({
    		filterdata: nextProps.filterdata,
    		filtermodeldata: nextProps.filtermodeldata,
            filterbodystyledata: nextProps.filterbodystyledata,
            settings: nextProps.settings,
    	})
        console.log('filterdataaaaaaaaaaaasasasasasas :'+JSON.stringify(nextProps.carsdata));
    }
    openform = (e) => {
    	// console.log('eeeeeeee'+JSON.stringify(e))
    	this.setState({carimg: e.featured_image,carname: e.car_name,car_id:e.car_id})
        window.$('#buynow').modal('show');

    }   
    setphone = (e) => {
        this.setState({Phone:e});
    }
    // onChangePage = (pageOfItems) => {
    //     this.setState({ pageOfItems: pageOfItems });
    // }
    submitform = (ev) => {
		let validate = this.formValidator;
        if(validate.allValid()){
        	this.setState({isSubmit:true});
            const {Name, Email, Phone, twelvemonthwarrenty, premiummax395, vrt_proccessing, transferuktodub, homedelivry, tnc, car_id} = this.state;
            const request =  new Request(`${apiBaseUrl}/submit-form`,{
                method: 'POST',
                headers: new Headers({ 'Content-Type': 'application/json','X-Auth-Token': `${localStorage.getItem('token')}`}),
                body: JSON.stringify({Name, Email, Phone, twelvemonthwarrenty, premiummax395, vrt_proccessing, transferuktodub, homedelivry, tnc, car_id})
            });
            return fetch(request).then(res => res.json()).then((data) => {
                // console.log('data :'+JSON.stringify(data.ResponseCode));
                if(data.ResponseCode == 1){
                    toastr.success('Form Submitted Successfully, We will get back to you soon.', { displayDuration: 3000 });
    				this.setState({carimg: '',carname: '',car_id: ''});
                    this.setState ({isSubmit : false})
        			window.$('#buynow').modal('hide');
                }
            }).catch(err => {
                this.setState ({isSubmit : false})
                console.log('err :'+err);
            })
        }else{
        	console.log('tnc: '+this.state.tnc)
        	validate.showMessages();
            this.forceUpdate();
        }
    }
    render() {
    	// console.log('totalcars: '+this.state.total_cars)
    	const { filter,carsloader,carsdata, carimg, carname,filterdata,filtermodeldata,filterbodystyledata,Year,Make,Model,Fuel,body_style,Condition,Mileage,transmission_type,engine,color } = this.state;
    	// console.log('YEAR'+Year);
        return (
            <>
            <div className="row wrapper border-bottom white-bg page-heading">
                <div className="col-lg-10">
                    <h2>Cars</h2>
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
                        </li>
                        <li className="breadcrumb-item active">
                            <NavLink className="nav-link" to="/vehicles">Cars</NavLink>
                        </li>
                    </ol>
                </div>
                <div className="col-lg-2">

                </div>
            </div>
            <div className="banner">
           	    <div className="row">
	  			<div className="col-md-3">
	  				<div className="left-sideblock">
	  					<form action="">
		  					<div className="search-box">					
							    <input className="form-control image" type="text" placeholder="Search"/>
		  					</div>
		  					<div className="car-filter">
		  						<h4>Cars Filters</h4>
		  						<label>Vechicles Matching</label>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="Year" onChange={e => this.onfilterChange(e)}>
								    <option value="">Year</option>
								    {
								    	filterdata.year
								    		?	filterdata.year.length > 0
								    				?
								    					filterdata.year.map((item) => (
								    						item.year != ''
								    							?
								    								<option xyz={Year} selected={Year == item.year ? true : ''} value={item.year}>{item.year} ({item.total})</option>
								    							:
								    								''
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="Make" onChange={e => this.onfilterChange(e)}>
								    <option value="">Make</option>
								    {
								    	filterdata.make
								    		?	filterdata.make.length > 0
								    				?
								    					filterdata.make.map((item) => (
								    						item.make != ''
								    							?
								    								<option selected={Make === item.make ? 'true' : ''} value={item.make}>{item.make} ({item.total})</option>
								    							:
								    								''
								    						
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="Model" onChange={e => this.onfilterChange(e)} disabled={Make ? false : true}>
								    <option value="">Model</option>
								    {
								    	filterdata.model
								    		?	filterdata.model.length > 0
								    				?
								    					filterdata.model.map((item) => (
								    						item.model != ''
								    							?
								    								<option selected={Model === item.model ? 'true' : ''} value={item.model}>{item.model} ({item.total})</option>
								    							:
								    								''	
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="body_style" onChange={e => this.onfilterChange(e)} disabled={Model ? false : true}>
								    <option value="">Body Style</option>
								    {
								    	filterdata.body_style
								    		?	filterdata.body_style.length > 0
								    				?
								    					filterdata.body_style.map((item) => (
								    						item.body_style != ''
								    							?
								    								<option selected={body_style === item.body_style ? 'true' : ''} value={item.body_style}>{item.body_style} ({item.total})</option>
								    							:
								    								''
								    						
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="Condition" onChange={e => this.onfilterChange(e)}>
								    <option value="">Condition</option>
								    {
								    	filterdata.make
								    		?	filterdata.car_condition.length > 0
								    				?
								    					filterdata.car_condition.map((item) => (
								    						item.car_condition != ''
								    							?
								    								<option selected={Condition === item.car_condition ? 'true' : ''} value={item.car_condition}>{item.car_condition} ({item.total})</option>
								    							:
								    								''
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="Mileage" onChange={e => this.onfilterChange(e)}>
								    <option value="">Milege</option>
								    {
								    	filterdata.year
								    		?	filterdata.mileage.length > 0
								    				?
								    					filterdata.mileage.map((item) => (
								    						item.mileage != ''
								    							?
								    								<option selected={Mileage === item.mileage ? 'true' : ''} value={item.mileage}>{item.mileage} ({item.total})</option>
								    							:
								    								''
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="transmission_type" onChange={e => this.onfilterChange(e)}>
								  	<option value="">Transmission</option>
								    {
								    	filterdata.transmission
								    		?	filterdata.transmission.length > 0
								    				?
								    					filterdata.transmission.map((item) => (
								    						item.transmission != ''
								    							?
								    								<option selected={transmission_type === item.transmission ? 'true' : ''} value={item.transmission}>{item.transmission} ({item.total})</option>
								    							:
								    								''
								    					))	
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="engine" onChange={e => this.onfilterChange(e)}>
								    <option value="">Engine</option>
								    {
								    	filterdata.engine
								    		?	filterdata.engine.length > 0
								    				?
								    					filterdata.engine.map((item) => (
								    						item.engine != ''
								    							?
								    								<option selected={engine === item.engine ? 'true' : ''} value={item.engine}>{item.engine} ({item.total})</option>
								    							:
								    								''
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="form-group">
								  <select className="form-control" id="sel1" name="Fuel" onChange={e => this.onfilterChange(e)}>
								    <option value="">Fuel Type</option>
								    {
								    	filterdata.fuel_type
								    		?	filterdata.fuel_type.length > 0
								    				?
								    					filterdata.fuel_type.map((item) => (
								    						item.fuel_type != ''
								    							?
								    								<option selected={Fuel === item.fuel_type ? 'true' : ''} value={item.fuel_type}>{item.fuel_type} ({item.total})</option>
								    							:
								    								''
								    					))
								    				:
								    					''
								    		:
								    			''
								    }
								  </select>
								</div>
								<div className="btnblock">
									 <button type="button" className="btn btn-info" onClick={e => this.resetFilters()} disabled={!filter}>Reset</button>
								</div>
		  					</div>
	  					</form>
	  				</div>
	  			</div>
	  			<div className="col-md-9">
	  				{ 
	  					carsloader
		  					? 	
		  						<div className="alltourticksloader">
	                            	<img className="loader_img" src="/assets/images/straight-loader.gif" />
	                        	</div>
	                        : carsdata
	                        	? carsdata.length > 0
                                    ? 
                                        carsdata.map((post) => (
                                        	<div className="right-sideblock">
												<div className="main">
													<div className="image-box">
														<img src={post.featured_image ? post.featured_image : ''} alt="car-image" className="img-fluid" />
														
														{post.car_condition == 'New'
															?
																<span>New</span>
															: ''
														}
													</div>
													<div className="content-box">
														<div className="left-cont">
															<h4>{post.car_name}</h4>
															{/* <p>{post.vehicle_overview}</p> */}
															{/*<del>1000.00</del> <span className="ml-2">£{post.price}</span>*/}
															<ul className="navbar-nav list">
																<li className="nav-item"><i className="fa fa-car mr-1"></i>{post.year}</li>
																<li className="nav-item"><i className="fa fa-sitemap mr-1"></i>{post.transmission}</li>
																<li className="nav-item"><i className="fa fa-tachometer mr-1"></i>{post.mileage}</li>
															</ul>
															{
																post.car_info 
																	? 
																		<div className="">
																			<table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
																				<tbody>
                                                                                    <tr className="strong">
																						<td>Exchange Rate:</td>
                                                                                        <td>{this.state.settings ?  this.state.settings.currency : ''}</td>
																					</tr>
                                                                                    <tr className="strong">
																						<td>Stat Code:</td>
																						<td>{post.stat_code}</td>
																					</tr>
																					<tr className="strong">
																						<td>CO2 Emission:</td>
																						<td>{post.co2_emission}</td>
																					</tr>
																					<tr className="strong">
																						<td>Engine Power</td>
																						<td>{post.enginePower}</td>
																					</tr>
                                                                                    <tr className="strong">
																						<td>EU Category:</td>
																						<td>{post.stat_code_cd_eu_cat}</td>
																					</tr>
                                                                                    <tr className="strong">
																						<td>Site Price:</td>
                                                                                        <td>{post.price}</td>
                                                                                    </tr>
                                                                                    <tr className="strong">
																						<td>Margin:</td>
                                                                                        <td>€{post.commission}</td>
                                                                                    </tr>
                                                                                    <tr className="strong">
																						<td>Registration Date:</td>
																						<td>{post.registration_date}</td>
																					</tr>
                                                                                    <tr className="strong">
																						<td>Fixed Price:</td>
																						<td>€{post.car_info.converted_price}</td>
																					</tr>
																					<tr className="strong">
																						<td>VRT:</td>
																					<td>€{post.car_info.co2_tax}</td>
																					</tr>
																					<tr className="strong">
																						<td>NOX TAX:</td>
																						<td>€{post.car_info.nox}</td>
																					</tr>
																					<tr className="strong">
																						<td>Total VRT:</td>
																						<td>€{post.car_info.nox+post.car_info.co2_tax}</td>
																					</tr>
																					<tr className="w3-border-top w3-border-bottom strong total">
																						<td class="w3-border-top">Trade&nbsp;Price:</td>
																						<td class="w3-border-top">€{post.car_info.final_price}</td>
																					</tr>
                                                                                    <tr className="strong">
																						<td>COMSP:</td>
																						<td>€{post.stat_code_comsp}</td>
																					</tr>
                                                                                    
																				</tbody>
																			</table>
																		</div> 
																	: 
																		<div class="btn-block mt-4">
																			<button type="button" className="btn btn-info">Call For Price</button>
																	  	</div>							
															}
														</div>
														<div className="btn-block">
															<a className="btn btn-info" target="_blank" href={`https://www.autotrader.co.uk/car-details/${post.car_id}`}>Autotrader Link</a>
															<p>.</p>
															<a className="btn btn-info" target="_blank" href={`/single-vehicle/${post.car_id}`}>Auto Merchant Link</a>

														</div>
													</div>
												</div>
											</div>	  				
                                        ))
                                    : <div>No Cars</div>
                                : <div>No Cars</div>
	  				}
	  				{ 
	  					this.props.carsloading
	  					? 
	  						''
	  					:
			  				<div className="pagination">
			                   	<Pagination
						          activePage={this.state.activePage}
						          itemsCountPerPage={25}
						          totalItemsCount={this.state.total_cars}
						          pageRangeDisplayed={5}
						          prevPageText="<"
						          nextPageText=">"
						          firstPageText="<<"
						          lastPageText=">>"
						          onChange={this.handlePageChange.bind(this)}
						        />
			                </div>
			        }
			        
	  			</div>
  			</div>
  			</div>
          </>
        )

    }
}

// export default withRouter(VehicleList);
const mapStateToProps = (state) => ({
    carsdata: state.car.carsdatanew,
    carsloading: state.car.carsloadingnew,
    filteredcardata: state.car.filteredcardatanew,
    filteredcarloading: state.car.filteredcarloadingnew,
    filterloading: state.car.filterloadingnew,
    filterdata: state.car.filterdatanew,
    filtermodelloading: state.car.filtermodelloading,
    filtermodeldata: state.car.filtermodeldata,
    filterbodystyleloading: state.car.filterbodystyleloading,
    filterbodystyledata: state.car.filterbodystyledata,
    settings: state.common.settings,
    settingsloading: state.common.settingsloading
})
export default connect(mapStateToProps)(AdminVehicleListnew);