import React, { Component } from 'react';
import { getcar } from '../store/actions/CarActions';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

class AdminSingleVehicle extends Component {
    constructor(props){
        super(props);
        this.state = {
			cardata:'',
			car_images:'',
			car_id:'',
		}
    }
    componentDidMount(){
        const car_id = this.props.match.params.id;
        this.setState({car_id: car_id});
        this.props.dispatch(getcar(car_id));
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            cardata: nextProps.cardata,
            car_images: nextProps.cardata.decoded_images,
        });
        console.log('cardata :'+JSON.stringify(nextProps.cardata));
    }
    render(){
        const { cardata,car_images } = this.state;
    	let i = 0;
    	let j = 0;
        return(
            <>
                <div className="row wrapper border-bottom white-bg page-heading">
                    <div className="col-lg-10">
                        <h2>Cars</h2>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
                            </li>
                            <li className="breadcrumb-item active">
                                <NavLink className="nav-link" to="">Car Detail</NavLink>
                            </li>
                        </ol>
                    </div>
                    <div className="col-lg-2">
                        
                    </div>
                </div>
                {this.props.carloading 
	  					? 	
	  						<div className="alltourticksloader">
                            	<img className="loader_img" src="/assets/images/straight-loader.gif" />
                        	</div>
                        : cardata
                            ? 
                                <>
                                <div className="social-links">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="border border-grey group w-100 d-md-flex py-1">
                                                <div className="col-lg-6 col-md-6 col-sm-12">
                                                    <button type="button" className="btn btn-light"><i className="fa fa-play" aria-hidden="true"></i>
                                                        Vehicle videos</button>

                                                    <button type="button" className="btn btn-light"><i className="fa fa-compress"
                                                            aria-hidden="true"></i>
                                                        Add to compare</button>

                                                </div>
                                                <div className="col-lg-6 col-md-6 col-sm-12">
                                                    <div className="social-lists align-items-center justify-content-center d-flex pull-right pt-2">
                                                        Share : <i className="fa fa-facebook" aria-hidden="true"></i>
                                                        <i className="fa fa-twitter" aria-hidden="true"></i>
                                                        <i className="fa fa-linkedin" aria-hidden="true"></i>
                                                        <i className="fa fa-google-plus" aria-hidden="true"></i>
                                                        <i className="fa fa-pinterest-p" aria-hidden="true"></i>
                                                        <i className="fa fa-whatsapp" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="py-2">
                                    <div className="row">
                                        <div className="col-md-8 my-4 ">
                                            <div className="" id="slider">
                                                <div id="myCarousel" className="carousel slide shadow">
                                                    <div className="carousel-inner">
                                                        {
                                                            car_images																	
                                                                ?	
                                                                    car_images.map((car_image) =>(
                                                                        // console.log('car_image:'+car_image.image)
                                                                        <div className={`carousel-item ${i == 0 ? 'active' : ''}`} data-slide-number={i++} >
                                                                            <img key={car_image.id} src={car_image.image} className="img-fluid"/>
                                                                        </div>
                                                                    ))
                                                                    
                                                                :
                                                                    ''
                                                        }
                                                        <a className="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                                                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span className="sr-only">Previous</span>
                                                        </a>
                                                        <a className="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                                                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span className="sr-only">Next</span>
                                                        </a>

                                                    </div>
                                                    {/* <!-- main slider carousel nav controls --> */}


                                                    <ul className="carousel-indicators list-inline mx-auto mt-2">
                                                        {car_images																	
                                                            ?	
                                                                car_images.map((car_image) =>(
                                                                    <li className="list-inline-item active">
                                                                        <a id={`carousel-selector-${car_image.id}`} className={j == 0 ? 'selected' : ''} data-slide-to={j++}
                                                                            data-target="#myCarousel">
                                                                            <img src={car_image.image} className="img-fluid"/>
                                                                        </a>
                                                                    </li>
                                                                ))
                                                                
                                                            :
                                                                ''
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                            {/* <!--/main slider carousel--> */}


                                            <div className="tab-section mt-5">
                                                <div className="container no-gutter">
                                                    <div className="row">
                                                        <div className="">
                                                            <div className="tabbable" id="tabs-211789">
                                                                <ul className="nav nav-tabs">
                                                                    <li className="nav-item">
                                                                        <a className="nav-link active" href="#tab1" data-toggle="tab"><i
                                                                                className="fa fa-sliders" aria-hidden="true"></i>
                                                                            Vehicle overview</a>
                                                                    </li>
                                                                    <li className="nav-item">
                                                                        <a className="nav-link" href="#tab2" data-toggle="tab"><i className="fa fa-list"
                                                                                aria-hidden="true"></i>
                                                                            Features & Options</a>
                                                                    </li>
                                                                    <li className="nav-item">
                                                                        <a className="nav-link" href="#tab3" data-toggle="tab"><i className="fa fa-list"
                                                                                aria-hidden="true"></i>
                                                                            Technical Specifications</a>
                                                                    </li>
                                                                </ul>
                                                                <div className="tab-content">
                                                                    <div className="tab-pane active" id="tab1">
                                                                        <p className="mt-4">
                                                                            {cardata.vehicle_overview}
                                                                        </p>
                                                                    </div>
                                                                    <div className="tab-pane" id="tab2">
                                                                        <h2>Features</h2>
                                                                        <ul className="featurelist">
                                                                            {
                                                                            cardata.features_options
                                                                                ?
                                                                                cardata.features_options.map((feature) =>(
                                                                                        // console.log('car_image:'+feature)
                                                                                        <><i class="fa fa-check-square-o" aria-hidden="true"></i> <li>{feature}</li></>
                                                                                        // <div className={`carousel-item ${i == 0 ? 'active' : ''}`} data-slide-number={i++} >
                                                                                        // 	<img key={car_image.id} src={car_image.image} className="img-fluid"/>
                                                                                        // </div>
                                                                                    ))	
                                                                                : 
                                                                                    ''
                                                                            }

                                                                        </ul>
                                                                    </div>
                                                                    <div className="tab-pane" id="tab3">
                                                                        <h2>Technical Specification</h2>
                                                                        <table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
                                                                            <tbody>
                                                        
                                                                                {
                                                                                    cardata.tech_info && cardata.tech_info.performance
                                                                                        ?
                                                                                        cardata.tech_info.performance.map((performance)=>(
                                                                                            <tr className="strong">
                                                                                                <td>{performance.name}:</td>
                                                                                                <td>{performance.value}</td>
                                                                                            </tr>
                                                                                        ))		
                                                                                        :
                                                                                            ''
                                                                                }
                                                                            </tbody>
                                                                        </table>
                                                                        <h2>Dimension</h2>
                                                                        <table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
                                                                            <tbody>
                                                        
                                                                                {
                                                                                    cardata.tech_info && cardata.tech_info.Dimensions
                                                                                        ?
                                                                                        cardata.tech_info.Dimensions.map((Dimensions)=>(
                                                                                            <tr className="strong">
                                                                                                <td>{Dimensions.name}:</td>
                                                                                                <td>{Dimensions.value}</td>
                                                                                            </tr>
                                                                                        ))		
                                                                                        :
                                                                                            ''
                                                                                }
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-4 mt-4">
                                            {
                                                cardata.car_info
                                                ?
                                                    <div className="price_section">
                                                        <table className="price-table w3-table w3-padding-0 w3-margin-0 w3-margin-bottom">
                                                            <tbody>
                                                                <tr className="strong">
                                                                    <td>Price:</td>
                                                                    <td>€{cardata.car_info.converted_price}</td>
                                                                </tr>
                                                                <tr className="strong">
                                                                    <td>CO2 VRT:</td>
                                                                <td>€{cardata.car_info.co2_tax}</td>
                                                                </tr>
                                                                <tr className="strong">
                                                                    <td>NOx VRT:</td>
                                                                    <td>€{cardata.car_info.nox}</td>
                                                                </tr>
                                                                <tr className="strong">
                                                                    <td>Total VRT:</td>
                                                                    <td>€{cardata.car_info.nox+cardata.car_info.co2_tax}</td>
                                                                </tr>
                                                                <tr className="w3-border-top w3-border-bottom strong total">
                                                                    <td class="w3-border-top">Trade&nbsp;Price:</td>
                                                                    <td class="w3-border-top">€{cardata.car_info.final_price}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>												
                                                :
                                                    <div class="btn-block">
                                                        <button type="button" className="btn btn-info">Call For Price</button>
                                                    </div>
                                            }
                                            <div className="description mt-4">
                                                <h4>Description</h4>

                                                <div className="container mt-4">

                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Year</div>
                                                            <div className="col-md-8">{cardata.car_year}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Make</div>
                                                            <div className="col-md-8">{cardata.make}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Body Style</div>
                                                            <div className="col-md-8">{cardata.body_style}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Condition</div>
                                                            <div className="col-md-8">{cardata.car_condition}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Mileage</div>
                                                            <div className="col-md-8">{cardata.mileage}</div>
                                                        </div>
                                                    </div>
                                                    {
                                                        cardata.vrm
                                                        ?
                                                            <div className="row">
                                                                <div className="border-bottom d-md-flex w-100 py-2">
                                                                    <div className="col-md-4">Registration Number</div>
                                                                    <div className="col-md-8">{cardata.vrm}</div>
                                                                </div>
                                                            </div>
                                                        :
                                                        ''
                                                    }
                                                    {/* {
                                                        cardata.vrm !== '' && cardata.vrm !== 0 
                                                        ?
                                                            <div className="row">
                                                                <div className="border-bottom d-md-flex w-100 py-2">
                                                                    <div className="col-md-4">Owner</div>
                                                                    <div className="col-md-8">{cardata.owner}</div>
                                                                </div>
                                                            </div>
                                                        :
                                                        ''
                                                    } */}
                                                    {
                                                        cardata.tax 
                                                        ?
                                                            <div className="row">
                                                                <div className="border-bottom d-md-flex w-100 py-2">
                                                                    <div className="col-md-4">Road Tax</div>
                                                                    <div className="col-md-8">€{cardata.tax}</div>
                                                                </div>
                                                            </div>
                                                        :
                                                        ''
                                                    }
                                                    {
                                                        cardata.car_doors 
                                                        ?
                                                            <div className="row">
                                                                <div className="border-bottom d-md-flex w-100 py-2">
                                                                    <div className="col-md-4">Doors</div>
                                                                    <div className="col-md-8">€{cardata.car_doors}</div>
                                                                </div>
                                                            </div>
                                                        :
                                                        ''
                                                    }
                                                    {
                                                        cardata.seats 
                                                        ?
                                                            <div className="row">
                                                                <div className="border-bottom d-md-flex w-100 py-2">
                                                                    <div className="col-md-4">Seats</div>
                                                                    <div className="col-md-8">{cardata.seats}</div>
                                                                </div>
                                                            </div>
                                                        :
                                                        ''
                                                    }
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Transmission</div>
                                                            <div className="col-md-8">{cardata.transmission}</div>
                                                        </div>
                                                    </div>
                                                    {/*<div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Drivetrain</div>
                                                            <div className="col-md-8">{cardata.drivetrain}</div>
                                                        </div>
                                                    </div>*/}
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Engine</div>
                                                            <div className="col-md-8">{cardata.engine}</div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Fuel Type</div>
                                                            <div className="col-md-8">{cardata.fuel_type}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Fuel Economy</div>
                                                            <div className="col-md-8">{cardata.fuel_economy}</div>
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="border-bottom d-md-flex w-100 py-2">
                                                            <div className="col-md-4">Trim</div>
                                                            <div className="col-md-8">{cardata.trim}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="fuel-efficiency pt-4">
                                                <h4>Fuel Efficiency</h4>
                                                <div className="fuel-economy mt-4">
                                                    <h2>Fuel Economy Rating</h2>
                                                    <div className="row">
                                                        <div className="city-info d-md-flex w-100">
                                                            <div className="col-md-4 text-center">
                                                                <p><small>City</small><br/><b>20</b></p>
                                                            </div>
                                                            <div className="col-md-4 text-center">
                                                                <i className="fa fa-car" aria-hidden="true"></i>
                                                            </div>
                                                            <div className="col-md-4 text-center">
                                                                <p><small>Highway</small><br/><b>26</b></p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <p>Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing
                                                        industries for previewing layouts and visual mockups.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </>
                            :
                                'No data'
                }            
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    cardata: state.car.cardata,
    carloading: state.car.carloading
})

export default connect(mapStateToProps)(AdminSingleVehicle);

// export default AdminSingleVehicle;